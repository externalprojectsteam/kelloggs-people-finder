using System;
using System.Collections.Generic;

namespace PeopleFinder.Shared
{
	public class NicknameEventArgs:EventArgs
	{
		public List<Nickname> nicknames {get;set;}
		public NicknameEventArgs (List<Nickname> nicknames)
		{
			this.nicknames = nicknames; 
		}
	}
}

