using KPeopleFinder.Model;

namespace PeopleFinder.Shared
{
	public class Person
	{
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string fullName { get; set; }
		public string title { get; set; }
		public string pictureURL { get; set; }
		public string workPhone { get; set; }
		public string mobilePhone { get; set; }
		public string email { get; set; }
		public string location { get; set; }
		public string aboutme { get; set; }
		public string skills { get; set; }
		public string askmeabout { get; set; }
		public string accountName { get; set; }



		//constructor
		public Person(string firstName, string lastName, string title, string pictureURL,
		              string workPhone, string mobilePhone, string email, string location,
		              string aboutme, string skills, string askmeabout)
		{
			this.firstName = firstName;
			this.lastName = lastName;
			this.title = title;
			this.pictureURL = pictureURL;
			this.workPhone = workPhone;
			this.mobilePhone = mobilePhone;
			this.email = email;
			this.location = location;
			this.aboutme = aboutme;
			this.skills = skills;
			this.askmeabout = askmeabout;
			this.fullName = this.firstName + " " + this.lastName;
		}
		public Person()
		{
		}


		public bool isMobilePhoneVisible
		{
			get
			{
				if (!string.IsNullOrWhiteSpace (mobilePhone))
					return true;
				else
					return false;
			}
		}

		//Determine through data binding in xaml whether to display work phone number
		public bool isWorkPhoneVisible
		{
			get
			{
				if (!string.IsNullOrWhiteSpace (workPhone))
					return true;
				else
					return false;
			}
		}

		public bool isEmailVisible
		{
			get
			{
				if (!string.IsNullOrWhiteSpace (email) && !email.Contains ("noemail"))
					return true;
				else
					return false;
			}
		}

		//if values in Location return true, otherwise return false.
		public bool isLocationVisible
		{
			get
			{
				if (!string.IsNullOrWhiteSpace(location))
					return true;
				else
					return false;
			}
		}

		//if values in AboutMe return true, otherwise return false.
		public bool isAboutMeVisibile
		{
			get
			{
				if (!string.IsNullOrWhiteSpace(aboutme))
					return true;
				else
					return false;
			}
		}

		//if values in SkillsVisible return true, otherwise return false
		public bool isSkillsVisible
		{
			get
			{
				if (!string.IsNullOrWhiteSpace(skills))
					return true;
				else
					return false;
			}
		}

		//if values in AskMeAbout return true, otherwise return false
		public bool isAskMeAboutVisible
		{
			get
			{
				if (!string.IsNullOrWhiteSpace(askmeabout))
					return true;
				else
					return false;
			}
		}
		//Determine whether they have any of the more info fields set in sharepoint.
		public bool isMoreInfoVisible
		{
			get
			{
				if (!(string.IsNullOrWhiteSpace(aboutme) && string.IsNullOrWhiteSpace(skills) && string.IsNullOrWhiteSpace(askmeabout)))
					return true;
				else
					return false;
			}
		}
		public string PictureURLWithAuth {
			get {
				//if they have a profile picture divert to the web address
				if (!string.IsNullOrWhiteSpace (pictureURL)) {
					return pictureURL.Replace (Constant.MY_SITE_URL, Constant.PeopleFinderURL);
				} 
				//Determine the file path for the default 'avatar' image. if silverlight determines if running
				//on WP8 else for us is iOS. They handle file paths to local folders different.
				#if SILVERLIGHT
				return @"/Images/avatar_l.png";
				#else
				return @"Images/avatar_l.png";
				#endif
			}
		}


	}




}
