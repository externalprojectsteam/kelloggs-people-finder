

namespace PeopleFinder.Shared
{
	public class Nickname
	{
		public string nickname { get; set; }
		public string fullname { get; set; }

		public Nickname(string nickname, string fullname)
		{
			this.nickname = nickname;
			this.fullname = fullname;
		}

	}

}
