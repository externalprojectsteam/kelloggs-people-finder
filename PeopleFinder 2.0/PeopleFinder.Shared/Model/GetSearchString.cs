using System;
using System.Collections.Generic;
using System.Linq;


namespace PeopleFinder.Shared
{
	public class GetSearchString
	{
		#region queryTemplate 
		string queryTemplate =
			@"&lt;QueryPacket Revision = ""1000""&gt;
                &lt;Query&gt;
                    &lt;Context&gt;
                        &lt;QueryText language=""en-US"" type = ""MSSQLFT""&gt;Select Rank, Title, Author, Size, Path, Write, AboutMe, Skills, preferredname, KIMSLocation,
                            HitHighlightedSummary, HitHighlightedProperties, accountname, pictureURL, workemail, workphone, jobtitle, firstname, lastname, mobilephone, BaseOfficeLocation 
                            FROM Scope() WHERE ##@#Searchtext#@## AND (""lastname"" IS NOT NULL) AND ((""SCOPE""='People')) ORDER  BY ""lastname"", ""firstname""
                        &lt;/QueryText&gt;
                    &lt;/Context&gt;
                   &lt;SupportedFormats Format=""urn:Microsoft.Search.Response.Document.Document"" /&gt;
                &lt;ResultProvider&gt;SharepointSearch&lt;/ResultProvider&gt;
                &lt;Range&gt;
                    &lt;StartAt&gt;1&lt;/StartAt&gt;
                    &lt;Count&gt;999&lt;/Count&gt;
                &lt;/Range&gt;
                &lt;EnableStemming&gt;true&lt;/EnableStemming&gt;
                &lt;EnableSpellCheck&gt;Suggest&lt;/EnableSpellCheck&gt;
                &lt;IncludeSpecialTermsResults&gt;true&lt;/IncludeSpecialTermsResults&gt;
                &lt;IncludeRelevantResults&gt;true&lt;/IncludeRelevantResults&gt;
                &lt;ImplicitAndBehavior&gt;true&lt;/ImplicitAndBehavior&gt;
                &lt;TrimDuplicates&gt;true&lt;/TrimDuplicates&gt;
                &lt;Properties&gt;
                  &lt;Property name=""Rank"" /&gt;
                  &lt;Property name=""Title"" /&gt;
                  &lt;Property name=""Author"" /&gt;
                  &lt;Property name=""Size"" /&gt;
                  &lt;Property name =""Path"" /&gt;
                  &lt;Property name =""Write""/&gt;
                  &lt;Property name =""HitHighlightedSummary""/&gt;
                  &lt;Property name =""HitHighlightedProperties""/&gt;
                  &lt;Property name=""accountname"" /&gt;
                  &lt;Property name=""pictureURL"" /&gt;
                  &lt;Property name=""workemail"" /&gt;
                  &lt;Property name=""workphone"" /&gt;
                  &lt;Property name=""jobtitle"" /&gt;
                  &lt;Property name=""firstname"" /&gt;
                  &lt;Property name=""lastname"" /&gt;
                  &lt;Property name=""mobilephone"" /&gt;
                  &lt;Property name=""AboutMe""/&gt;
                  &lt;Property name=""Skills""/&gt;
                  &lt;Property name=""PreferredName""/&gt;
				  &lt;Property name=""BaseOfficeLocation""/&gt;
				  &lt;Property name=""KIMSLocation""/&gt;
                &lt;/Properties&gt;
              &lt;/Query&gt;
            &lt;/QueryPacket&gt;";
		#endregion

		public string getSearchString (string firstName, string lastName, string phone, string email, string skills, string location, string basicSearchText, List<Nickname> allNicknames)
		{
			if (firstName == null)
				firstName = string.Empty;
			if (lastName == null)
				lastName = string.Empty;
			if (phone == null)
				phone = string.Empty;
			if (email == null)
				email = string.Empty;
			if (skills == null)
				skills = string.Empty;

			String[] possibleNames;
			bool basicSearchFlag = false;
			List<String> sortedNicknames = new List<String>();
			bool digit = true;
			for (int j = 0; j < phone.Length; j++)
			{
				if (!char.IsDigit(phone[j]) && phone[j] != ' ' && phone[j] != '+')
					digit = false;
			}
			if (!phone.Equals (string.Empty) && basicSearchText.Equals(string.Empty) && digit) {
				basicSearchText = phone;
			}
			// Manage the single search box, try to split apart the name and search in different ways.
			// Try to figure out what you are searching for
			if (!basicSearchText.Equals(""))
			{
				possibleNames = basicSearchText.Split(' ');

				if (possibleNames.Count() > 2)
				{
					// Search as one string
					lastName = basicSearchText;
					basicSearchFlag = true;
				}
				else if (possibleNames.Count() == 2)
				{
					// Split apart first and last names, add wildcards and nickname searches
					firstName = possibleNames[0].ToLower();
					lastName = possibleNames[1].ToLower();
					//basicSearchFlag = true;
					// Bug with searching searches such as 'Tom T' all names coming up due to DefaultProperties search, makes this not occur.
					//if (lastName.Count() == 1)
						basicSearchText = "";
				}
				else
				{
					// Search the result as everything!
					basicSearchFlag = true;

					firstName = possibleNames[0];
					lastName = possibleNames[0];
					email = possibleNames[0];
					skills = possibleNames[0];
				}

			}
			int i = -1;

			// If they search by first name, check the name searched against nicknames to see if there are full names
			// which we can search for also, etc search 'Tom' will find the full name 'Thomas'
			if (!firstName.Equals(""))
			{
				//Binary search our List of Nicknames with our full name, try and find a match!
				i = allNicknames.BinarySearch(new Nickname(firstName.ToUpper(), ""), new CompareCustomDataType());
			}

			// if we get a result, get all the nicknames possibly associated
			if (i > 0)
			{
				sortedNicknames.Add(allNicknames[i].fullname);
				while (true)
				{
					i++;
					if (firstName.ToUpper().Equals(allNicknames[i].nickname))
					{
						sortedNicknames.Add(allNicknames[i].fullname);
					}
					else
						break;
				}
			}
			string queryBuilder = ""; // form our query based upon user input
			bool andCheck = false; // check if there has been a previous statement (whether to put 'AND');
			string searchType = "AND";
			if (basicSearchFlag) // if the single 'Basic' search box is selected, do an OR search rather than AND
				searchType = "OR";

			if (!basicSearchText.Equals(""))
			{
				queryBuilder += "(FreeText(DefaultProperties, '" + lastName + "*')";
				andCheck = true;
			}
			if (!lastName.Equals("Last Name") && !lastName.Equals(string.Empty))
			{
				if (andCheck == true)
					queryBuilder += " OR Contains(lastname, '\"" + lastName + "*\"')) ";
				else
				{
					queryBuilder += "Contains(lastname, '\"" + lastName + "*\"') ";
					andCheck = true;
				}
			}
			if (!firstName.Equals("First Name") && !firstName.Equals(string.Empty))
			{
				if (andCheck == true && sortedNicknames.Count() > 0)
					queryBuilder += searchType + " (Contains(firstname, '\"" + firstName + "*\"') ";
				else if (andCheck == false && sortedNicknames.Count > 0)
				{
					queryBuilder += "(Contains(firstname, '\"" + firstName + "*\"') ";
					andCheck = true;
				}
				else if (andCheck == false)
				{
					queryBuilder += "Contains(firstname, '\"" + firstName + "*\"') ";
					andCheck = true;
				}
				else
					queryBuilder += searchType +" Contains(firstname, '\"" + firstName + "*\"') ";
			}
			for (int j = 0; j < sortedNicknames.Count(); j++)
			{
				queryBuilder += "OR Contains(firstname, '\"" + sortedNicknames[j] + "\"') ";
			}
			if (sortedNicknames.Count > 0)
				queryBuilder += ")";
			if (!email.Equals("Email") && !email.Equals(string.Empty))
			{
				if (andCheck == true)
					queryBuilder += searchType + " (\"workemail\" LIKE '%" + email + "%')";
				else
				{
					queryBuilder += "(\"workemail\" LIKE '%" +email + "%')";
					andCheck = true;
				}
			}
			if (!location.Equals("Location") && !location.Equals(string.Empty))
			{
				if (andCheck == true)
					queryBuilder += searchType + "Contains(BaseOfficeLocation, '\"" + location + "*\"') ";
				else
				{
					queryBuilder += "Contains(BaseOfficeLocation, '\"" + location + "*\"') ";
					andCheck = true;
				}
			}
			if (!phone.Equals("Phone Number") && !phone.Equals(string.Empty))
			{
				if (andCheck == true)
					queryBuilder += searchType +" ((\"workphone\" LIKE '%" + phone + "%') OR " +
						"(\"mobilephone\" LIKE '%" + phone + "%'))";
				else
					queryBuilder += "Contains(workphone, '\"" + phone + "*\"') ";

			}
			if (!skills.Equals("Skills") && !skills.Equals(string.Empty))
			{
				if (andCheck == true)
					queryBuilder += searchType + " Contains(Skills, '\"" + skills + "*\"')";
				else
					queryBuilder += "Contains(Skills, '\"" + skills + "*\"') ";
			}
			return queryTemplate.Replace("##@#Searchtext#@##", queryBuilder);
		}

	}
}

