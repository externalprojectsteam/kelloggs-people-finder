using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPeopleFinder.Model
{
	/// <summary>
	/// Constant values used throughout the search progress.
	/// </summary>
	class Constant
	{
		public const int PAGE_SIZE = 5;
		public const string USER_ID = "_PeopleFinder";
		public const string PASSWORD = "Droisys12";
		public const string MY_SITE_URL = "http://mysite.kellogg.com";
		public const string PEOPLE_FINDER_URL = "https://{0}:{1}@mobilepeoplefinder.kellogg.com";
		public const string PEOPLE_FINDER_SEARCH = "https://mobilepeoplefinder.kellogg.com/_vti_bin/search.asmx";


		public static string PeopleFinderURL { get { return string.Format(PEOPLE_FINDER_URL, USER_ID, PASSWORD); } }

	}
}
