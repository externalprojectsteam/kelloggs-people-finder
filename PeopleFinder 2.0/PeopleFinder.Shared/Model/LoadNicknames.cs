using System.ComponentModel;
using System.Collections.Generic;
using System;
using System.IO;

namespace PeopleFinder.Shared
{
	public class LoadNicknames
	{
		private BackgroundWorker bw = new BackgroundWorker();
		public static List<Nickname> allNicknames = new List<Nickname>();
		string[] words;

		public LoadNicknames ()
		{
			bw.WorkerReportsProgress = true;
			bw.WorkerSupportsCancellation = true;
			bw.DoWork += new DoWorkEventHandler(bw_DoWork);
			bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_WorkComplete);
			bw.RunWorkerAsync();
				// Perform any additional setup after loading the view, typically from a nib.
			}
			private void bw_DoWork(object sender, DoWorkEventArgs e)
			{
				try
				{
					using (StreamReader sr = new StreamReader("Model/Misc/nicknames.txt"))
					{
						// read line by line splitting by tab.
						words = sr.ReadToEnd().Split(null);
					}

					// Seperate Array of single words into an Array of nickname objects 
					for (int i = 0; i < words.Length; i++)
					{   // First element
						if (i == 0 && !words[i].Equals("")) 
						{
							// First element and second form a nickname and a fullname
							allNicknames.Add(new Nickname (words[i], words[i+1]));
						}
						// There is a blank space between each pair of nickname/fullname
						else if (words[i].Equals(""))
						{
							try{
								allNicknames.Add(new Nickname (words[i+1], words[i+2]));
							}
							catch(Exception){}
						}
					}
				}
				catch (Exception error)
				{
					Console.WriteLine("This file could not be read:");
					Console.WriteLine(error.Message);
				}
			}
			private void bw_WorkComplete(object sender, RunWorkerCompletedEventArgs e)
			{
			}
		}
	}

