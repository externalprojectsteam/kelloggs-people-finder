using System;
using System.Text;
using KPeopleFinder.Model;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ComponentModel;
using System.Net.Http;
using System.IO;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Collections.Generic;
using System.Linq;

namespace PeopleFinder.Shared
{
	public class SearchSharePoint
	{
		public static event EventHandler<SearchEventArgs> searchComplete;
		string xmLText;
		static string _responseString = null;  
		string basicSearchString;   
		private static HttpWebRequest request;
		static HttpWebRequest spAuthReq;

		public SearchSharePoint (string searchString, string basic)
		{
			xmLText = searchString;
			basicSearchString = basic;
			Authenticate ();
		}

		/// <summary>
		/// Attempt to bomb the current search
		/// </summary>
		public static void stop()
		{
			try{
			_responseString = null;
			spAuthReq.Abort ();
			request.Abort ();
			}catch(Exception){
			}
			throw new Exception ();
		}
	
		private void Authenticate()
		{
			System.Uri authServiceUri = new Uri(Constant.PEOPLE_FINDER_SEARCH);

			spAuthReq = HttpWebRequest.Create(authServiceUri) as HttpWebRequest;
			var bc = Encoding.UTF8.GetBytes(string.Format(@"{0}:{1}", Constant.USER_ID, Constant.PASSWORD));
			//establish all the headers for our search request
			spAuthReq.Headers[HttpRequestHeader.Authorization] = "Basic " + Convert.ToBase64String(bc);
			spAuthReq.Headers["SOAPAction"] = "urn:Microsoft.Search/Query";
			spAuthReq.ContentType = "text/xml; charset=utf-8";
			spAuthReq.Method = "POST";
			spAuthReq.BeginGetRequestStream(new AsyncCallback(spSearchReqCallBack), spAuthReq);
		}
		/// <summary>
		/// 'Posts' the search string to the sharepoint page
		/// </summary>
		/// 
		private void spSearchReqCallBack(IAsyncResult asyncResult)
		{
			string envelope = @"<?xml version=""1.0"" encoding=""utf-8""?>
                                    <s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/""><s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                                        <Query xmlns='urn:Microsoft.Search'>
                                            <queryXml>
                                                {0}
                                            </queryXml>
                                        </Query>
                                      </s:Body>
                                    </s:Envelope>";
			UTF8Encoding encoding = new UTF8Encoding();
			request = (HttpWebRequest)asyncResult.AsyncState;
			Stream _body = request.EndGetRequestStream(asyncResult);
			envelope = string.Format(envelope, xmLText);
			byte[] formBytes = encoding.GetBytes(envelope);
			_body.Write(formBytes, 0, formBytes.Length);
			_body.Close();
			try{
			request.BeginGetResponse(new AsyncCallback(SearchCallback), request);
			}catch (Exception){
			}
		}
		private void SearchCallback(IAsyncResult asyncResult)
		{
			try{ // protection when we bomb threads
			HttpWebRequest request = null;
			HttpWebResponse response = null;
			Stream content = null;
			try{
				request = (HttpWebRequest)asyncResult.AsyncState;
				response = (HttpWebResponse)request.EndGetResponse(asyncResult);
				content = response.GetResponseStream();
			}catch (Exception){
				_responseString = null;
			}

			if (request != null && response != null)
			{
				if (response.StatusCode == HttpStatusCode.OK)
				{
					using (StreamReader reader = new StreamReader(content))
					{
						//Put debugging code here
						_responseString = reader.ReadToEnd();
						reader.Close();
					}
				}
			}

			if (_responseString != null) {
				string test = null;
				string finalString = null;
				test = _responseString.Replace ("&lt;", "<");
				finalString = test.Replace ("&gt;", ">");
				Person person = new Person ();
				List<Person> persons = new List<Person> ();
				List<Person> lastNameMatchList = new List<Person>();
			
				using (XmlReader reader = XmlReader.Create(new StringReader(finalString))) {
				
					while (reader.Read ()) {
						// Only detect start elements.
						if (reader.IsStartElement ()) {
								//Parse through the results, assign appropriate tags to our person object
							if (reader.Name.Equals("Name"))
							{
								reader.Read ();
								switch (reader.Value) {
								case "BASEOFFICELOCATION":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.location = reader.Value;
									break;
								case "PICTUREURL":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.pictureURL = reader.Value;
									break;
								case "WORKEMAIL":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.email = reader.Value.ToLower ();
									break;
								case "WORKPHONE":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.workPhone = reader.Value;
									break;
								case "JOBTITLE":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.title = FixCase(reader.Value.ToLower(), false);
									break;
								case "MOBILEPHONE":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.mobilePhone = reader.Value;
									break;
								case "FIRSTNAME":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.firstName = FixCase (reader.Value.ToLower(), false);
									break;
								case "LASTNAME":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.lastName = FixCase (reader.Value.ToLower(), false);
									break;
								case "PREFERREDNAME":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.fullName = FixCase (reader.Value.ToLower (), true);
									break;
								case "ABOUTME":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.aboutme = reader.Value;
									break;
								case "SKILLS":
									while (!reader.Name.Equals ("Value")) {
										reader.Read ();
									}
									reader.Read ();
									person.skills = reader.Value;
									break;
								}

							}
						}
						if (!reader.IsStartElement()) {
							if (reader.Name.Equals("Document")){
								if (person.firstName != null && person.lastName != null && person.email != null && person.title != null) {
									if (person.lastName.ToLower ().Equals (basicSearchString.ToLower ()))
										lastNameMatchList.Add (person);
									else
										persons.Add (person);
								}
								person = new Person ();
							}
						}
					}
				}
				List<Person> combinedList = lastNameMatchList.Union(persons).ToList();

				searchComplete(this, new SearchEventArgs(combinedList));
				
			}
			}catch(Exception) {}


		}

        /// <summary>
        /// Attempt to fix the case to make names/titles more normalized
        /// </summary>
        /// <param name="input">The string to be fixed</param>
        /// <param name="flipNames">If it is a name with a comma, flip the name on the comma</param>
        /// <returns></returns>
		private string FixCase(string input, bool flipNames)
		{
			if (flipNames) {
				string[] flipNamesString = input.Split (',');
				if (flipNamesString.Count () == 2) {
					input = flipNamesString [1].Trim () + " " + flipNamesString [0].Trim ();
				}
			}
			string[] names = input.Split(' '); // if multiple names are used, split them to capitalize (Ex. Anne Marie)
			string result = "";
			// Loop through all names, wheter multiple or one
			foreach (string name in names)
			{
				string currentName = name.Trim();

				// If there is a ' in a name then capitalize appropriatley (ex. O'Hare)
				if (currentName.Contains ('\'')) {
					try {
						int pos = currentName.IndexOf ('\'');
						string newname = "";
						currentName = currentName.First ().ToString ().ToUpper () + String.Join ("", currentName.Skip (1)) + " ";
						for (int i = 0; i <= pos; i++) {
							newname += currentName [i];
						}
						result += newname + currentName [pos + 1].ToString ().ToUpper () + String.Join ("", currentName.Substring (pos + 2));
					} catch (Exception) {
						result += currentName.First ().ToString ().ToUpper () + String.Join ("", currentName.Skip (1)) + " ";
					}
				}
				// if there is a - in a name capitalize appropratley
				else if (currentName.Contains ('-')) {
					try {
						int pos = currentName.IndexOf ('-');
						string newname = "";
						currentName = currentName.First ().ToString ().ToUpper () + String.Join ("", currentName.Skip (1)) + " ";
						for (int i = 0; i <= pos; i++) {
							newname += currentName [i];
						}
						result += newname + currentName [pos + 1].ToString ().ToUpper () + String.Join ("", currentName.Substring (pos + 2));
					} catch (Exception) {
						result += currentName.First ().ToString ().ToUpper () + String.Join ("", currentName.Skip (1)) + " ";
					}
				} else if (currentName.Contains ('(')) {
					try {
						int pos = currentName.IndexOf ('(');
						string newname = "";
						currentName = currentName.First ().ToString ().ToUpper () + String.Join ("", currentName.Skip (1)) + " ";
						for (int i = 0; i <= pos; i++) {
							newname += currentName [i];
						}
						result = result.Trim ();
						result += " " + newname + currentName [pos + 1].ToString ().ToUpper () + String.Join ("", currentName.Substring (pos + 2));
					} catch (Exception) {
						result += currentName.First ().ToString ().ToUpper () + String.Join ("", currentName.Skip (1)) + " ";
					}
					//fix capitalization on numerals
				} else if (currentName.Equals ("ii") || currentName.Equals ("ii,") || currentName.Equals ("iii") || currentName.Equals ("iii,")
				           || currentName.Equals ("iv") || currentName.Equals ("iv,")) {
					if (currentName.Equals ("ii"))
						currentName = "II";
					else if (currentName.Equals ("ii,"))
						currentName = "II,";
					else if (currentName.Equals ("iii"))
						currentName = "III";
					else if (currentName.Equals ("iii,"))
						currentName = "III,";
					else if (currentName.Equals ("iv"))
						currentName = "IV";
					else if (currentName.Equals ("iv,"))
						currentName = "IV,";
					result += currentName + " ";
					//fix capitalizations on some common job titles
				} else if (currentName.Equals ("vp") || currentName.Equals ("vp,") || currentName.Equals ("u.s.") || currentName.Equals ("u.s.,")
				         || currentName.Equals ("qa") || currentName.Equals ("qa,") || currentName.Equals ("svp") || currentName.Equals ("svp,")
					|| currentName.Equals("ceo,") || currentName.Equals("research/quality/tech") || currentName.Equals("it")) {
					if (currentName.Equals ("vp"))
						currentName = "VP";
					else if (currentName.Equals ("vp,"))
						currentName = "VP,";
					else if (currentName.Equals ("u.s."))
						currentName = "U.S.";
					else if (currentName.Equals ("u.s.,"))
						currentName = "U.S.,";
					else if (currentName.Equals ("qa"))
						currentName = "QA";
					else if (currentName.Equals ("qa,"))
						currentName = "QA,";
					else if (currentName.Equals ("svp"))
						currentName = "SVP";
					else if (currentName.Equals ("svp,"))
						currentName = "SVP,";
					else if (currentName.Equals ("ceo,"))
						currentName = "CEO,";
					else if (currentName.Equals ("research/quality/tech"))
						currentName = "Research/Quality/Tech";
					else if (currentName.Equals ("it"))
						currentName = "IT";

					result += currentName + " ";
				}

//					result += currentName + " ";

				else {
					// make the appropriate upper case letters
					try {
						result += currentName.First ().ToString ().ToUpper () + String.Join ("", currentName.Skip (1)) + " ";
					} catch (Exception) {
						result += currentName+ " ";
					}
				}
				}

			return result.Trim();
			}

			
		}



	}



