using System;
using System.Collections.Generic;

namespace PeopleFinder.Shared
{
	public class SearchEventArgs: EventArgs
	{
		//Event args allowing us to pass back all the results from our search via Event Handler.
		public SearchEventArgs (List<Person> persons)
		{
			this.persons = persons;
		}
		public List<Person> persons {get; set;}
	}
}

