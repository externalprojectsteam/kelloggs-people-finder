using System.Collections.Generic;
using System;

namespace PeopleFinder.Shared
{
	public class CompareCustomDataType : IComparer<Nickname>
	{
		//compare the nickname of two NickName objects, determine if they are the same.

		//In our case, when we perform a Binary Search we pass the firstName in and try and find
		//nicknames
		public int Compare(Nickname x, Nickname y)
		{
			if (x == y) return 0;
			if (x == null) return -1;
			if (y == null) return 1;

			return String.Compare(x.nickname, y.nickname);
		}
	}
}

