﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using KPeopleFinder.Model;
using PeopleFinder.Shared;

namespace KPeopleFinder
{
    public partial class AdvancedSearchResultsPage : PhoneApplicationPage
    {
        BitmapImage background;
        private List<Person> contacts;

        public AdvancedSearchResultsPage()
        {
            InitializeComponent();

            SystemTray.ProgressIndicator = null;

            //Create sample data
            List<Person> k = PhoneApplicationService.Current.State["SearchResults"] as List<Person>;
            if (k != null)
                ContactsList.ItemsSource = k;
            if (k.Count == 0 || k == null)
                ErrorText.Visibility = Visibility.Visible;
           
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            background = new BitmapImage(new Uri("Images/Backgrounds/bg_result-wvga.png", UriKind.Relative));
            App.RootFrame.Background = new ImageBrush() { ImageSource = background };
        }

        private void ContactsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PhoneApplicationService.Current.State["Person"] = (Person)ContactsList.SelectedItem;
            this.NavigationService.Navigate(new Uri("/Result.xaml", UriKind.Relative));
        }

    }
}