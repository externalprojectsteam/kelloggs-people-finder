﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace KPeopleFinder
{
    public partial class LoginPage : PhoneApplicationPage
    {
        BitmapImage background;
        bool usernameFocused;

        public LoginPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            background = new BitmapImage(new Uri("Images/Backgrounds/bg_login-wxga.png", UriKind.Relative));
            App.RootFrame.Background = new ImageBrush() { ImageSource = background };
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }

        private void username_GotFocus(object sender, RoutedEventArgs e)
        {
            usernameFocused = true;
            if (username.Text.Equals("User Name", StringComparison.OrdinalIgnoreCase))
            {
                username.Text = string.Empty;
            }
        }

        private void username_LostFocus(object sender, RoutedEventArgs e)
        {
            usernameFocused = false;
            if (string.IsNullOrEmpty(username.Text))
            {
                username.Text = "User Name";
            }
        }

    }
}