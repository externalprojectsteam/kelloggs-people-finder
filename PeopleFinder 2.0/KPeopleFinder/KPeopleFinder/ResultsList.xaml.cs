﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using KPeopleFinder.Model;
using System.ComponentModel;
using PeopleFinder.Shared;

namespace KPeopleFinder
{
    public partial class ResultsList : PhoneApplicationPage
    {
        public static bool networkerror = false;
        public static List<Person> results;
        public ResultsList()
        {
            InitializeComponent();
            ErrorReport.Visibility = Visibility.Collapsed;

            while (results == null){}
            
            if (networkerror)
            {
                ErrorReport.Text = "Network Error!\r\n\r\nCheck your network or \r\nretry a more specific search.";
                ErrorReport.Visibility = Visibility.Visible;
                networkerror = false;
            }
            else if (results.Count == 0)
            {
                ErrorReport.Text = "No Results Found\r\n\r\nTry a different or \r\nmore specific search.";
                ErrorReport.Visibility = Visibility.Visible;
            }
             
            //************************
            // CREATE BUTTONS FROM RESULTS ARRAY OF PERSONS 
            //************************
            #region createButtons
            int marginLeft = 0;
            int marginTop = 0;
            const int BUTTON_DIM = 235;
            const int BUTTON_MARG = 221;
            const int PHOTO_HEIGHT = 100;
            const int PHOTO_WIDTH = 81;
            const int NAME_FONT = 18;
            const int TITLE_FONT = 14;

            String[] colors = { "#BBDB7093", "#DDAF1E2D" };
            int colorcount = 0;
            int resultCount = results.Count;
            if (resultCount != 0)
                ErrorReport.Visibility = Visibility.Collapsed;

            bool switchFlag = true;

            for (int i = 0; i < resultCount; i++)
            {
                // ensure no empty results are displayed
                try
                {
                    if (results[i].firstName.Equals("") || results[i].lastName.Equals(""))
                        continue;
                }
                catch
                {
                    continue;
                }
                // Define a button
                Button button = new Button();
                button.Margin = new Thickness(marginLeft, marginTop, 0, 0);
                button.Height = BUTTON_DIM;
                button.Width = BUTTON_DIM;
                //Name the button its position in the array, we can find out which button was clicked even
                //when they all go to the same event
                button.Name = i + "";
                //Create a grid to design the button
                Grid buttonGrid = new Grid();
                // Define the image within the button
                Image photoHolder = new Image();
                photoHolder.Height = PHOTO_HEIGHT;
                photoHolder.Source = new BitmapImage(new Uri(results[i].PictureURLWithAuth, UriKind.RelativeOrAbsolute));
                photoHolder.Width = PHOTO_WIDTH;
                photoHolder.Margin = new Thickness(-4, -24, 64, 66);
                buttonGrid.Children.Add(photoHolder);
                StackPanel sp = new StackPanel();
                //Define where the name is held within the button
                TextBlock nameHolder = new TextBlock();
                nameHolder.FontSize = NAME_FONT;
                nameHolder.TextWrapping = TextWrapping.Wrap;
                nameHolder.Margin = new Thickness(0, 100, 0, 0);
                nameHolder.TextAlignment = TextAlignment.Center;
                nameHolder.Text = results[i].fullName;
                sp.Children.Add(nameHolder);
                // define where the title is held within the button
                TextBlock titleHolder = new TextBlock();
                titleHolder.FontSize = TITLE_FONT;
                titleHolder.TextWrapping = TextWrapping.Wrap;
                titleHolder.Margin = new Thickness(0, 5, 0, 0);
                titleHolder.Text = results[i].title;
                titleHolder.TextAlignment = TextAlignment.Center;
                sp.Children.Add(titleHolder);
                buttonGrid.Children.Add(sp);
                // add the grid to the button
                button.Content = buttonGrid;
                button.BorderThickness = new Thickness(0);
                // set button color, make them interweaved
                button.Background = GetColorFromHexa(colors[colorcount]);
                button.Click += resultClicked;
                button.VerticalAlignment = VerticalAlignment.Top;

                if (switchFlag)
                {
                    // Change color
                    if (colorcount == 1)
                        colorcount = 0;
                    else
                        colorcount = 1;
                    // Align button correctly, increase the margin appropriately
                    button.HorizontalAlignment = HorizontalAlignment.Left;
                    marginLeft += BUTTON_MARG;
                    switchFlag = false;
                }
                else
                {
                    switchFlag = true;
                    // change margin to align button appropriately
                    marginLeft -= BUTTON_MARG;
                    marginTop += BUTTON_MARG;
                }
                // Add button to our grid in a scroll viewer
                PersonGrid.Children.Add(button);
            }
            SystemTray.ProgressIndicator.IsVisible = false;
            #endregion
        }

        private void resultClicked(object sender, RoutedEventArgs e)
        {
            // get the button name, this correlates with its position in the array
            string buttonName = ((Button) sender).Name;
            int resultNum = Convert.ToInt32(buttonName);
            // pass the person object to the Result view to display the data appropriately
            PhoneApplicationService.Current.State["Person"] = results[resultNum];
            NavigationService.Navigate(new Uri("/Result.xaml", UriKind.Relative));
        }
        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            results = null;
        }
        public static SolidColorBrush GetColorFromHexa(string hexaColor)
        {
            return new SolidColorBrush(
                Color.FromArgb(
                    Convert.ToByte(hexaColor.Substring(1, 2), 16),
                    Convert.ToByte(hexaColor.Substring(3, 2), 16),
                    Convert.ToByte(hexaColor.Substring(5, 2), 16),
                    Convert.ToByte(hexaColor.Substring(7, 2), 16)
                )
            );
        }
    }
}