﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using KPeopleFinder.Resources;
using KPeopleFinder.Model;
using System.IO;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using PeopleFinder.Shared;
using System.Windows.Input;
using System.Net.NetworkInformation;

namespace KPeopleFinder
{
    public partial class MainPage : PhoneApplicationPage
    {
        //Ensure that the placeholders/hints are still correct after clear button is clicked
        bool fNameFocused = false;
        bool lNameFocused = false;
        bool phoneFocused = false;
        bool emailFocused = false;
       // bool locationFocused = false;
        bool skillsFocused = false;
        bool basicSearchFocused = false;
        string firstNameText, lastNameText, phoneText, emailAddressText, skillsText; // basicSearchText;
        private BackgroundWorker bw = new BackgroundWorker();
        List<Nickname> allNicknames = new List<Nickname>();

        BitmapImage backgroundSearch;
        BitmapImage backgroundAdvanced;

       
        String[] words;
         public static string xmlText;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Remove login page from the back stack
            if (App.RootFrame.BackStack.Count() > 0)
            {
                if (App.RootFrame.BackStack.ElementAt(0).Source.ToString().StartsWith("/LoginPage.xaml") == true) App.RootFrame.RemoveBackEntry();
            }

            clearFields();
           
            //Initialize background images for root frame (for full screen backgrounds)
            backgroundSearch = new BitmapImage(new Uri("Images/Backgrounds/bg_search_wvga.jpg", UriKind.Relative));
            backgroundAdvanced = new BitmapImage(new Uri("Images/Backgrounds/bg_advsearch-wxga.png", UriKind.Relative));

            switch (MainPivot.SelectedIndex)
            {
                case 0:
                    App.RootFrame.Background = new ImageBrush() { ImageSource = backgroundSearch };
                    break;
                case 1:
                    App.RootFrame.Background = new ImageBrush() { ImageSource = backgroundAdvanced };
                    break;
            }

            var content = Application.Current.Host.Content;

            double scale = (double)content.ScaleFactor / 100;

            int h = (int)Math.Ceiling(content.ActualHeight * scale);

            int w = (int)Math.Ceiling(content.ActualWidth * scale);

            Size resolution = new Size(w, h);

            if (resolution.Height == 1280 && resolution.Width == 720)
            {
                placeholder.Margin = new Thickness(0, 60, 0, 0);
            }
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.RunWorkerAsync();

        }

        //Changes backgroung image as you switch between pivot items
        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch ((sender as Pivot).SelectedIndex)
            {
                case 0:
                    App.RootFrame.Background = new ImageBrush() { ImageSource = backgroundSearch };
                    break;
                case 1:
                    App.RootFrame.Background = new ImageBrush() { ImageSource = backgroundAdvanced };
                    break;
            }
        }

        //check if any of the advanced search fields have values
        private bool TextFieldsChanged()
        {
            if (fName.Text.Trim().Equals("First Name") && lName.Text.Trim().Equals("Last Name") && phone.Text.Trim().Equals("Phone Number")
                && email.Text.Trim().Equals("Email") && skills.Text.Trim().Equals("Skills"))
                return false;
            else
                return true;
        }

        //Load common nicknames file into objects in the background, allow for more dynamic searching
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            try
            {
                using (StreamReader sr = new StreamReader("Model/nicknames.txt"))
                {
                    // read line by line splitting by tab.
                     words = sr.ReadToEnd().Split(null);
                }

                // Seperate Array of single words into an Array of nickname objects 
                for (int i = 0; i < words.Length; i++)
                {   // First element
                    if (i == 0 && !words[i].Equals(""))
                    {
                        // First element and second form a nickname and a fullname
                        allNicknames.Add(new Nickname (words[i], words[i+1]));
                    }
                    // There is a blank space between each pair of nickname/fullname
                    else if (words[i].Equals(""))
                    {
                        try{
                            allNicknames.Add(new Nickname (words[i+1], words[i+2]));
                        }
                        catch(Exception){}
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine("This file could not be read:");
                Console.WriteLine(error.Message);
            }
        }


        /// <summary>
        /// Either the Advanced Search button is clicked or the user selected the enter on the keyboard.
        /// </summary>
        private void AdvancedSearchButton_Click(object sender, EventArgs e)
        {
            if (TextFieldsChanged())
            {
                if (!fName.Text.Trim().Equals("First Name"))
                    firstNameText = fName.Text.Trim();
                else
                    firstNameText = "";

                if (!lName.Text.Trim().Equals("Last Name"))
                    lastNameText = lName.Text.Trim();
                else
                    lastNameText = "";

                if (!phone.Equals("Phone Number"))
                    phoneText = phone.Text.Trim();
                else
                    phoneText = "";

                if (!email.Text.Trim().Equals("Email"))
                    emailAddressText = email.Text.Trim();
                else
                    emailAddressText = "";

                if (!skills.Text.Trim().Equals("Skills"))
                    skillsText = skills.Text.Trim();
                else
                    skillsText = "";

                try
                {
                    SystemTray.ProgressIndicator = new ProgressIndicator
                    {
                        IsIndeterminate = true,
                        IsVisible = true,
                        Text = "Searching..."
                    };
                    GetSearchString search = new GetSearchString();
                    string s = search.getSearchString(firstNameText, lastNameText, phoneText, emailAddressText, skillsText, "", "", allNicknames);
                    new SearchSharePoint(s, "");

                    SearchSharePoint.searchComplete += SearchSharePoint_searchComplete;

                    if (NetworkInterface.GetIsNetworkAvailable() == false)
                    {
                        SystemTray.SetProgressIndicator(this, null);
                        MessageBox.Show("No Network Connection!");
                        SearchSharePoint.stop();
                    }
                }
                catch (Exception) { }

            }
        }

        /// <summary>
        /// Method called from the event handler in SearchSharepoint, launch the results view pasing in the results
        /// </summary>
        private void SearchSharePoint_searchComplete(object sender, SearchEventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                PhoneApplicationService.Current.State["SearchResults"] = e.persons;
                NavigationService.Navigate(new Uri("/AdvancedSearchResultsPage.xaml", UriKind.Relative));
            });
        }

        // Handle placeholders / hints. When someone  focuses on a text field, the field clears
        // when someone clicks away if they have not entered anything the hint reappears
        // Windows does not do this automatically so we must code these.
        #region Placeholers/hints


        /// <summary>
        /// Clears out all text fields to their original value. 
        /// Performs when navigated to page
        /// </summary>
        private void clearFields()
        {
            if (!fNameFocused)
                fName.Text = "First Name";
            else
                fName.Text = string.Empty;
            if (!lNameFocused)
                lName.Text = "Last Name";
            else
                lName.Text = string.Empty;
            if (!phoneFocused)
                phone.Text = "Phone Number";
            else
                phone.Text = string.Empty;
            if (!emailFocused)
                email.Text = "Email";
            else
                email.Text = string.Empty;
            if (!skillsFocused)
                skills.Text = "Skills";
            else
                skills.Text = string.Empty;
            if (!basicSearchFocused)
                basicSearch.Text = "Search";
            else
                basicSearch.Text = string.Empty;
        }


        private void fName_GotFocus(object sender, RoutedEventArgs e)
        {
            fNameFocused = true;
            if (fName.Text.Equals("First Name", StringComparison.OrdinalIgnoreCase))
            {
                fName.Text = string.Empty;

            }
            basicSearch.Text = "Search";
        }

        private void fName_LostFocus(object sender, RoutedEventArgs e)
        {
            fNameFocused = false;
            if (string.IsNullOrEmpty(fName.Text))
            {
               fName.Text = "First Name";
            }

        }
        private void lName_GotFocus(object sender, RoutedEventArgs e)
        {
            lNameFocused = true;
            if (lName.Text.Equals("Last Name", StringComparison.OrdinalIgnoreCase))
            {
                lName.Text = string.Empty;
            }
            basicSearch.Text = "Search";
        }
        private void lName_LostFocus(object sender, RoutedEventArgs e)
        {
            lNameFocused = false;
            if (string.IsNullOrEmpty(lName.Text))
            {
                lName.Text = "Last Name";
            }
        }
        private void phone_GotFocus(object sender, RoutedEventArgs e)
        {
            phoneFocused = true;
            if (phone.Text.Equals("Phone Number", StringComparison.OrdinalIgnoreCase))
            {
                phone.Text = string.Empty;
            }
            basicSearch.Text = "Search";
        }
        private void phone_LostFocus(object sender, RoutedEventArgs e)
        {
            phoneFocused = false;
            if (string.IsNullOrEmpty(phone.Text))
            {
                phone.Text = "Phone Number";
            }
        }
        private void email_GotFocus(object sender, RoutedEventArgs e)
        {
            emailFocused = true;
            if (email.Text.Equals("Email", StringComparison.OrdinalIgnoreCase))
            {
                email.Text = string.Empty;
            }
            basicSearch.Text = "Search";
        }
        private void email_LostFocus(object sender, RoutedEventArgs e)
        {
            emailFocused = false;
            if (string.IsNullOrEmpty(email.Text))
            {
                email.Text = "Email";
            }
        }
        /*
        private void location_GotFocus(object sender, RoutedEventArgs e)
        {
            locationFocused = true;
            if (location.Text.Equals("Location", StringComparison.OrdinalIgnoreCase))
            {
                location.Text = string.Empty;
            }
            basicSearch.Text = "Search";
        }
        private void location_LostFocus(object sender, RoutedEventArgs e)
        {
            locationFocused = false;
            if (string.IsNullOrEmpty(location.Text))
            {
                location.Text = "Location";
            }
        }*/

        private void skills_GotFocus(object sender, RoutedEventArgs e)
        {
            skillsFocused = true;
            if (skills.Text.Equals("Skills", StringComparison.OrdinalIgnoreCase))
            {
                skills.Text = string.Empty;
            }
            basicSearch.Text = "Search";
        }
        private void skills_LostFocus(object sender, RoutedEventArgs e)
        {
            skillsFocused = false;
            if (string.IsNullOrEmpty(skills.Text))
            {
                skills.Text = "Skills";
            }
        }
        private void basicSearch_GotFocus(object sender, RoutedEventArgs e)
        {
            basicSearchFocused = true;
            if (basicSearch.Text.Equals("Search", StringComparison.OrdinalIgnoreCase))
            {
                basicSearch.Text = string.Empty;
            }
            fName.Text = "First Name";
            lName.Text = "Last Name";
            phone.Text = "Phone Number";
            email.Text = "Email";
          //  location.Text = "Location";
            skills.Text = "Skills";
        }
        private void basicSearch_LostFocus(object sender, RoutedEventArgs e)
        {
            basicSearchFocused = false;
            if (string.IsNullOrEmpty(basicSearch.Text))
            {
                basicSearch.Text = "Search";
            }
        }

        #endregion


        /// <summary>
        /// TODO: Not implemented in v1.0. Recently search contacts
        /// </summary>
        private void RecentContactsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Result.xaml", UriKind.Relative));
            RecentContactsList.SelectedItem = null;
        }

        /// <summary>
        /// Perform the search for basic search
        /// </summary>
        private void search()
        {
             SystemTray.ProgressIndicator = new ProgressIndicator
                    {
                        IsIndeterminate = true,
                        IsVisible = true,
                        Text = "Searching..."
                    };
                    try
                    {
                        GetSearchString search = new GetSearchString();
                        string s = search.getSearchString("", "", "", "", "", "", basicSearch.Text.Trim(), allNicknames);
                        new SearchSharePoint(s, basicSearch.Text.Trim());

                        SearchSharePoint.searchComplete += SearchSharePoint_searchComplete;
                        //attempt to handle network issues
                        if (NetworkInterface.GetIsNetworkAvailable() == false)
                        {
                            SystemTray.SetProgressIndicator(this, null);
                            MessageBox.Show("No Network Connection!");
                            SearchSharePoint.stop();
                        }
                    }
                    catch (Exception) { 
                        //if search failed, check network
                     if (NetworkInterface.GetIsNetworkAvailable() == false)
                        {
                            SystemTray.SetProgressIndicator(this, null);
                            MessageBox.Show("No Network Connection!");
                            SearchSharePoint.stop();
                        }
                    }
                }
        

        #region KeyboardEnterPressed
        /// <summary>
        /// Enter is pressed on the basic search text field, perform search.
        /// </summary>
        private void basicSearch_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!basicSearch.Text.Trim().Equals("Search")){
                    search();
                }      
            }
        }

        private void fName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AdvancedSearchButton_Click(null, null);
            }
        }

        private void lName_KeyDown(object sender, KeyEventArgs e)
        {
             if (e.Key == Key.Enter)
            {
                AdvancedSearchButton_Click(null, null);
            }
        }

        private void phone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AdvancedSearchButton_Click(null, null);
            }
        }

        private void email_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AdvancedSearchButton_Click(null, null);
            }
        }

        private void skills_keyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AdvancedSearchButton_Click(null, null);
            }
        }

        #endregion




    }
}