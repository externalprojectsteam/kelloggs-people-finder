﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Media;


namespace KPeopleFinder.UserControls
{
    public enum Position { first, only, last, between, none }

    public partial class OrganizationChartItemLevel2 : UserControl
    {


        public OrganizationChartItemLevel2(Position position, params object[] list)
        {
            InitializeComponent();

            switch (position)
            {
                case Position.first:
                case Position.last:
                case Position.only:
                    linkImage.Source = new BitmapImage(new Uri("/Images/Chart/chart_b-wvga.png", UriKind.Relative));
                    break;
                case Position.between:
                    linkImage.Source = new BitmapImage(new Uri("/Images/Chart/chart_dw_b-wvga.png", UriKind.Relative));
                    break;
                case Position.none:
                    break;
            }

            if (list.Count() > 0)
            {
                if (list[0].ToString() == "person") nameTextBox.Foreground = new SolidColorBrush(Color.FromArgb(255, 226, 0, 52));
            }
        }
    }
}
