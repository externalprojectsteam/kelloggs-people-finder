﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;

namespace KPeopleFinder.UserControls
{
    public partial class OrganizationChartItemLevel1 : UserControl
    {
        public OrganizationChartItemLevel1(params object[] list)
        {
            InitializeComponent();

            if (list.Count() > 0)
            {
                if (list[0].ToString() == "person") nameTextBox.Foreground = new SolidColorBrush(Color.FromArgb(255, 226, 0, 52));
            }
        }
    }
}
