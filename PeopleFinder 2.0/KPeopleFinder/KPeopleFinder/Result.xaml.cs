﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Phone.Tasks;
using KPeopleFinder.Model;
using KPeopleFinder.UserControls;
using PeopleFinder.Shared;

namespace KPeopleFinder
{
    public partial class Results : PhoneApplicationPage
    {
        Person ps;
        BitmapImage background;

        public Results()
        {
            InitializeComponent();

            //Create sample data
            ps = PhoneApplicationService.Current.State["Person"] as Person;
            this.DataContext = ps;
        

           /*
            coWorkers.Add(new Person("Anne Marie", "Delgato", "SENIOR EXECUTIVE SECRETARY", "/Images/SampleImages/Sample1.png", null, null, null, null, null, null, null, null, null, null));
            coWorkers.Add(new Person("Jack", "Hagist", "VP, GLOBAL IT APPLICATION DELIVERY", "/Images/SampleImages/Sample2.png", null, null, null, null, null, null, null, null, null, null));
            coWorkers.Add(new Person("Bill", "Hutchinson", "VP, BUSINESS MANAGEMENT", null, null, null, null, null, null, null, null, null, null, null));
            coWorkers.Add(new Person("Diana", "Karkins", "VP, APPLICATION SOLUTIONS", "/Images/SampleImages/Sample3.png", null, null, null, null, null, null, null, null, null, null));
            coWorkers.Add(new Person("Gurinder", "Kaur", "VP, GLOBAL IT MKT & SELL BUS SOLUTIONS", null, null, null, null, null, null, null, null, null, null, null));
            coWorkers.Add(new Person("Art", "Mason", "VP, IT STRATEGY PLANNING & PROGRAMS", null, null, null, null, null, null, null, null, null, null, null));
            coWorkers.Add(new MoPerson("Salvador", "Millan", "VP, GLOBAL INFRASTRUCTURE OPERATIONS", null, null, null, null, null, null, null, null, null, null, null));
            coWorkers.Add(new Model.Person("Steven", "Young", "CHIEF INFORMATION SECURITY OFFICER", null, null, null, null, null, null, null, null, null, null, null));
            List<Person> employees = new List<Person>();
            employees.Add(new Model.Person("Ed", "Hanson", "SR DIR. APPLICATION", null, null, null, null, null, null, null, null, null, null, null));
            employees.Add(new Model.Person("Julie", "Sharpsteen", "DIR. APPLICATION", null, null, null, null, null, null, null, null, null, null, null));
            employees.Add(new Model.Person("Ken", "Thompson", "DIR. APPLICATION", null, null, null, null, null, null, null, null, null, null, null));
         */

            //=================================
            // PROFILE PAGE
            //=================================
        if (!ps.isMobilePhoneVisible)
        {
            mobileLabel.Visibility = Visibility.Collapsed;
            mobilephone.Visibility = Visibility.Collapsed;
            textLabel.Visibility = Visibility.Collapsed;
            text.Visibility = Visibility.Collapsed;
         }
         if (!ps.isWorkPhoneVisible)
         {
             workphone.Visibility = Visibility.Collapsed;
             workLabel.Visibility = Visibility.Collapsed;
         }
         if (!ps.isEmailVisible)
         {
               emailLabel.Visibility = Visibility.Collapsed;
                email.Visibility = Visibility.Collapsed;
          }
         if (!ps.isLocationVisible)
         {
               locationLabel.Visibility = Visibility.Collapsed;
               location.Visibility = Visibility.Collapsed;
         }
        

       /* private void DrawOrganizationChart()
        {
            if (ps.manager == null)
            {
                UserControls.OrganizationChartItemLevel1 personItem = new UserControls.OrganizationChartItemLevel1() { DataContext = ps };
                orgStack.Children.Add(personItem);

                orgStack.Children.Add(new Image() { Source = new BitmapImage(new Uri("/Images/Chart/chart_dw-wvga.png", UriKind.Relative)), HorizontalAlignment = System.Windows.HorizontalAlignment.Left, Width=80 });

                if (ps.employees.Count > 0)
                {
                    foreach (Person employee in ps.employees)
                    {
                        if (employee == ps.employees.ElementAt(ps.employees.Count - 1)) orgStack.Children.Add(new UserControls.OrganizationChartItemLevel2(Position.last) { DataContext = employee });
                        else orgStack.Children.Add(new UserControls.OrganizationChartItemLevel2(Position.between) { DataContext = employee });

                    }
                }
            }
            else if (ps.manager != null)
            {
                orgStack.Children.Add(new UserControls.OrganizationChartItemLevel1() { DataContext = ps.manager });

                orgStack.Children.Add(new Image() { Source = new BitmapImage(new Uri("/Images/Chart/chart_dw-wvga.png", UriKind.Relative)), HorizontalAlignment = System.Windows.HorizontalAlignment.Left, Width = 80 });


                if (ps.coWorker.Count > 0)
                {
                    UserControls.OrganizationChartItemLevel2 personItem = new UserControls.OrganizationChartItemLevel2(Position.between, "person") { DataContext = ps };
                    orgStack.Children.Add(personItem);

                    foreach (Person coWorker in ps.coWorker)
                    {
                        if (coWorker == ps.coWorker.ElementAt(ps.coWorker.Count-1)) orgStack.Children.Add(new UserControls.OrganizationChartItemLevel2(Position.last) { DataContext = coWorker });
                        else orgStack.Children.Add(new UserControls.OrganizationChartItemLevel2(Position.between) { DataContext = coWorker });
                    }
                }
          }
         
        } */

        ////==================================
        //// ORGANIZATION PANORAMIC
        ////===================================
        //#region OrganizationPanoramic
        //// Keep track of margin, formats appropriatley even if no boss
        //int marginLeft = 10;
        //// Write out boss, if none then does not write
        //try
        //{
        //    string bossFullName = ps.manager.lastName + ", " + ps.manager.firstName;
        //    TextBlock bossName = new TextBlock();
        //    bossName.Margin = new Thickness(marginLeft, 10, 0, 0);
        //    marginLeft += 10;
        //    bossName.Text = bossFullName;

        //    TextBlock bossTitle = new TextBlock();
        //    bossTitle.Margin = new Thickness(marginLeft, 0, 0, 0);
        //    marginLeft += 30;
        //    bossTitle.Text = ps.manager.title;
        //    orgStack.Children.Add(bossName);
        //    orgStack.Children.Add(bossTitle);
        //}
        //catch (Exception) // no boss
        //{ // no output
        //}
        //try
        //{
        //    //Write out coworker equals
        //    for (int i = 0; i < ps.coWorker.Count(); i++)
        //    {
        //        TextBlock coworkerName = new TextBlock();
        //        coworkerName.Margin = new Thickness (marginLeft, 10, 0, 0);
        //        coworkerName.Text = ps.coWorker[i].lastName + ", " + ps.coWorker[i].firstName;
        //        TextBlock coworkerTitle = new TextBlock();
        //        coworkerTitle.TextWrapping = TextWrapping.Wrap;
        //        coworkerTitle.Margin = new Thickness (marginLeft + 10,0,0,0);
        //        coworkerTitle.Text = ps.coWorker[i].title;
        //        orgStack.Children.Add(coworkerName);
        //        orgStack.Children.Add(coworkerTitle);
        //    }
        //}
        //catch (Exception) // no coworker
        //{ // no output
        //}
        ////write out your name
        //TextBlock personName = new TextBlock();
        //personName.Margin = new Thickness(marginLeft, 10, 0, 0);
        //marginLeft += 10;
        //personName.Text = ps.lastName + ", " + ps.firstName;
        //TextBlock persontitle = new TextBlock();
        //persontitle.TextWrapping = TextWrapping.Wrap;
        //persontitle.Margin = new Thickness(marginLeft, 0, 0, 0);
        //persontitle.Text = ps.title;
        //orgStack.Children.Add(personName);
        //orgStack.Children.Add(persontitle);
        //marginLeft += 30;
        //try
        //{
        //    //write out your employees
        //    for (int i = 0; i < ps.employees.Count(); i++)
        //    {
        //        TextBlock employeeName = new TextBlock();
        //        employeeName.Margin = new Thickness(marginLeft, 10, 0, 0);
        //        employeeName.TextWrapping = TextWrapping.Wrap;
        //        employeeName.Text = ps.employees[i].lastName + ", " + ps.employees[i].firstName;
        //        TextBlock employeeTitle = new TextBlock();
        //        employeeTitle.Margin = new Thickness(marginLeft+10, 0, 0, 0);
        //        employeeTitle.TextWrapping = TextWrapping.Wrap;
        //        employeeTitle.Text = ps.employees[i].title;
        //        orgStack.Children.Add(employeeName);
        //        orgStack.Children.Add(employeeTitle);
        //    }
        //}
        //catch (Exception) // no employees
        // { // no output
        // }

        //#endregion

        //================================
        // MORE INFORMATION PANORAMIC
        //===============================
         results.Items.Remove(orgChart);
        if (!ps.isMoreInfoVisible)
        {
            results.Items.Remove(moreInfo);
        }
        if (!ps.isSkillsVisible)
        {
            skills.Visibility = Visibility.Collapsed;
            skillslabel.Visibility = Visibility.Collapsed;
        }
        if (!ps.isAskMeAboutVisible)
        {
            askmeaboutlabel.Visibility = Visibility.Collapsed;
            askmeabout.Visibility = Visibility.Collapsed;
         }
        }
        /*
                private static List<Person> GetUserColleagues(Person person)
                {
                    List<Person> coWorker;
                    UserProfileWebService.localhost.ContactData[] contacts = myService.GetUserColleages(person.accountName);
                    for (int i = 0; i < contacts.Length; i++)
                    {
                        coWorker.Add(contacts[i]);
                    }
                    return coWorker;
                }
        */

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize background images for root frame (for full screen backgrounds)
            background = new BitmapImage(new Uri("Images/Backgrounds/bg_result-wvga.png", UriKind.Relative));
            App.RootFrame.Background = new ImageBrush() { ImageSource = background };

           // DrawOrganizationChart();
        }


        //// Adds pinch zoom to image
        //private void Image_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        //{
        //    if (e.PinchManipulation != null)
        //    {
        //        var transform = (CompositeTransform)profileImagecompress.RenderTransform;

        //        // Scale Manipulation
        //        transform.ScaleX = e.PinchManipulation.CumulativeScale;
        //        transform.ScaleY = e.PinchManipulation.CumulativeScale;

        //        // Translate manipulation
        //        var originalCenter = e.PinchManipulation.Original.Center;
        //        var newCenter = e.PinchManipulation.Current.Center;
        //        transform.TranslateX = newCenter.X - originalCenter.X;
        //        transform.TranslateY = newCenter.Y - originalCenter.Y;


        //        // end 
        //        e.Handled = true;
        //    }
            
           
        //}
        #region Tasks
        //Call mobile phone
        private void mobileCallClick(object sender, MouseButtonEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();
            phoneCallTask.DisplayName = ps.firstName + " " + ps.lastName;
            phoneCallTask.PhoneNumber = ps.mobilePhone;
            phoneCallTask.Show();
        }
       
        //text mobile phone
        private void mobileTextClick(object sender, MouseButtonEventArgs e)
        {
            SmsComposeTask smsComposeTask = new SmsComposeTask();
            smsComposeTask.To = ps.mobilePhone ;
            smsComposeTask.Show();
        }
        // call work phone
        private void workCallClick(object sender, MouseButtonEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();
            phoneCallTask.DisplayName = ps.firstName + " " + ps.lastName;
            phoneCallTask.PhoneNumber = ps.workPhone;
            phoneCallTask.Show();
        }
        // email 
        private void emailClick(object sender, MouseButtonEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.To = ps.email;
            emailComposeTask.Show();
        }
        //add contact
        private void addContact(object sender, EventArgs e)
        {
            SaveContactTask saveContactTask = new SaveContactTask();
            saveContactTask.JobTitle = ps.title;
            saveContactTask.FirstName = ps.firstName;
            saveContactTask.LastName = ps.lastName;
            if (!ps.mobilePhone.Equals(string.Empty))
            {
                saveContactTask.MobilePhone = ps.mobilePhone;
            }
            if (!ps.workPhone.Equals(string.Empty))
            {
                saveContactTask.WorkPhone = ps.workPhone;
            }
            saveContactTask.WorkEmail = ps.email;
            saveContactTask.WorkAddressStreet = ps.location;

            saveContactTask.Show();
        }

        //shows address on map
        private void addressClick(object sender, MouseButtonEventArgs e)
        {
            BingMapsTask bingMapsTask = new BingMapsTask();

            bingMapsTask.SearchTerm = ps.location;
            bingMapsTask.ZoomLevel = 2;

            bingMapsTask.Show();
        }
        #endregion


    }
}