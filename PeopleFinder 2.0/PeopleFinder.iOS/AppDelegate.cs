using UIKit;
using Foundation;
using PeopleFinder.Shared;
using System;


namespace PeopleFinder.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register ("AppDelegate")]


	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		public override UIWindow Window {
			get;
			set;
		}
		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
		}
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
		}

		/// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
		}

		/// This method is called when the application is about to terminate. Save data, if needed. 
		public override void WillTerminate (UIApplication application)
		{
		}
		public override bool WillFinishLaunching (UIApplication application, NSDictionary launchOptions)
		{
			var style = UINavigationBar.AppearanceWhenContainedIn (typeof(KelloggNavigationViewController));
			style.SetBackgroundImage (Resources.NavBarBackgroundImage, UIBarMetrics.Default);
			style.ShadowImage = new UIImage ();


			UINavigationBar.Appearance.SetBackgroundImage(Resources.NavBarBackgroundImage, UIBarMetrics.Default);
//			var customSearchButton = UIButton.AppearanceWhenContainedIn (typeof(UISearchBar));
//			customSearchButton.SetTitleColor(UIColor.FromRGB(255,255,255), UIControlState.Normal);
			//UINavigationBar.Appearance.ShadowImage = new UIImage ();
			new LoadNicknames ();
			return true;
		}

	}
}

