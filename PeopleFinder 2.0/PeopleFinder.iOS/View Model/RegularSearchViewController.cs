using UIKit;
using System;
using PeopleFinder.Shared;
using Foundation;
using CoreGraphics;
using System.Collections.Generic;

namespace PeopleFinder.iOS
{

	public partial class RegularSearchViewController : UIViewController
	{
		AdvancedViewController advancedViewController;
		private Person personTouched;
		private UITableView table;
		private UISearchBar searchBar;
		private UINavigationItem navItem;
		UIBarButtonItem advancedCancel;
		UIImageView placeholder;
		UIImageView searchHint;
		UIView shadowView;
		String searchText;
		UIAlertView alert;
		static nfloat navBarHeight = 0;
		bool searchPressed = false;
		bool toChild = false;
		bool tableShowing = false;
		List<Person> results = new List<Person>();
		NSIndexPath rowPressed = null;
		int adjuster = 0;

		//used for iOS6/iOS7 seperation
		nfloat mainViewHeight = 0;
		nfloat searchHeight = 0;
		//Find Device Version
		string s = (UIDevice.CurrentDevice.SystemVersion);

		public RegularSearchViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		
			// For iOS 7 Note: Also used in EditStart

			if (Convert.ToInt16(s.Split('.')[0]) > 6) {
				//Set view controller title
				nfloat labelSize = Resources.NavBarLogo.Size.Width + expectedLabelWidth (new NSString ("People Finder"), 17);	UIView titleView = new UIView (new CGRect (0, 0, 300, 40));
				//Create the K logo
				UIImageView logoImage = new UIImageView (Resources.NavBarLogo);
				logoImage.Frame = new CGRect (this.View.Frame.Width / 2 - labelSize / 2, 7, Resources.NavBarLogo.Size.Width, Resources.NavBarLogo.Size.Height);

				// create the "People Finder" text
				UILabel logoText = new UILabel (new CGRect (120, 7, 280, Resources.NavBarLogo.Size.Height));
				logoText.Text = Resources.NavBarText;
				logoText.TextColor = UIColor.FromRGB (255, 255, 255);
				logoText.Font = UIFont.FromName ("HelveticaNeue", 17f);

				titleView.AddSubview (logoImage);
				titleView.BackgroundColor = UIColor.Clear;
				titleView.AddSubview (logoText);
				NavigationItem.TitleView = titleView;
				mainViewHeight = UIApplication.SharedApplication.StatusBarFrame.Height + NavigationController.NavigationBar.Frame.Height + navBar.Frame.Height;
				searchHeight = mainViewHeight - navBarHeight;
				adjuster += 18;
				// For iOS6
			} else {
				UIImage backgroundImage = Resources.NavBarBackgroundImage;

				NavigationController.NavigationBar.SetBackgroundImage (backgroundImage.Scale(new CGSize(View.Frame.Width, 44)), UIBarMetrics.Default);
				UIStringAttributes titleTextAttributes = new UIStringAttributes();
				titleTextAttributes.Font = UIFont.FromName("HelveticaNeue", 17);
				titleTextAttributes.ForegroundColor = UIColor.FromRGB (255, 255, 255);
				titleTextAttributes.Shadow = new NSShadow();
				titleTextAttributes.Shadow.ShadowColor = UIColor.Clear;
				NavigationController.NavigationBar.TitleTextAttributes = titleTextAttributes;

				nfloat labelSize = Resources.NavBarLogo.Size.Width + expectedLabelWidth (new NSString ("People Finder"), 17);
				UIView titleView = new UIView (new CGRect (0, 0, 300, 44));
				//Create the K logo
				UIImageView logoImage = new UIImageView (Resources.NavBarLogo);
				logoImage.Frame = new CGRect (this.View.Frame.Width / 2 - 65, 7, Resources.NavBarLogo.Size.Width, Resources.NavBarLogo.Size.Height);
				logoImage.BackgroundColor = UIColor.Clear;
				// create the "People Finder" text
				UILabel logoText = new UILabel (new CGRect (120, 7, 280, Resources.NavBarLogo.Size.Height));
				logoText.BackgroundColor = UIColor.Clear;
				logoText.Text = Resources.NavBarText;
				logoText.TextColor = UIColor.FromRGB (255, 255, 255);
				logoText.Font = UIFont.FromName ("HelveticaNeue", 17f);

				titleView.AddSubview (logoImage);
				titleView.BackgroundColor = UIColor.Clear;
				titleView.AddSubview (logoText);
				NavigationItem.TitleView = titleView;

				navBar.Frame = new CGRect (0, 0, navBar.Frame.Width, navBar.Frame.Height);
				mainViewHeight = 0 + navBar.Frame.Height;
				searchHeight = navBar.Frame.Height * 2 + 5;
			}

			//Create a nav bar to hold the search bar so we can customize the button
			navBar.SetBackgroundImage (Resources.SearchBarBackgroundImage, UIBarMetrics.Default);
			navItem = new UINavigationItem ();
			navItem.SetHidesBackButton (true, false);

			//Create our search bar, button arrange bar in center, button to right
			searchBar = new UISearchBar (new CGRect(0,0, 250, navBar.Frame.Height-5));
			searchBar.BackgroundImage = new UIImage ();
			searchBar.Placeholder = "Search";
			navItem.TitleView = searchBar;

			advancedCancel = new UIBarButtonItem ();
			advancedCancel.Title = "Advanced";
			UITextAttributes textAttr = new UITextAttributes ();
			textAttr.Font = UIFont.FromName ("HelveticaNeue", 14f);
			textAttr.TextColor = UIColor.FromRGB (255, 255, 255);
			textAttr.TextShadowColor = UIColor.Clear;
			advancedCancel.SetTitleTextAttributes (textAttr, UIControlState.Normal);
			advancedCancel.SetBackgroundImage (new UIImage (), UIControlState.Normal, UIBarMetrics.Default);
			navItem.RightBarButtonItem = advancedCancel;
			navBar.PushNavigationItem (navItem, false);
			navBarHeight = navBar.Frame.Height;

			searchBar.Layer.BorderWidth = 0;

			//Configure the rest of the views setup
			mainViewSetup();
		}
		public override void ViewWillAppear(bool animated)
		{
			// if we have come back from a person detail screen
			if (toChild) {
				// try and fix the table view which likes to move

				mainView.RemoveFromSuperview ();
				mainView = new UIView(new CGRect (0, searchHeight - navBarHeight, View.Frame.Width, View.Frame.Height - (searchHeight + navBarHeight) + 50));
				//mainView.Frame = new RectangleF(0,searchHeight - navBarHeight,View.Frame.Width, mainView.Frame.Height + 35);
				NavigationController.SetNavigationBarHidden (true, true);
				//Display the results in our table using SearchViewSource for setup.
				table.RemoveFromSuperview ();
				table = new UITableView(new CGRect(0, 18, mainView.Frame.Width, View.Frame.Height  - (searchHeight - navBarHeight) - adjuster), UITableViewStyle.Plain);
				clearMainView();
				// Add the table we created to our mainView.
				mainView.Add(table);
				advancedCancel.Title = "Cancel";
				// UI custom separator
				table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				table.Source = new SearchViewSource (results);
				tableShowing = true;
				toChild = false;
				View.Add (mainView);
				table.ScrollToRow (rowPressed, UITableViewScrollPosition.Top, false);

			} 
			//bug fix for the table from advanced search sometimes intefering
			else {
				try{
					table.RemoveFromSuperview ();
					mainViewSetup();
					tableShowing = false;
				}
				catch(Exception){
				}
			}
		}
		public override void ViewWillDisappear(bool animated)
		{
			tableShowing = false;
		}
		public override void ViewDidAppear(bool animated)
		{
			// Add our event delegates
			searchBar.SearchButtonClicked += searchButtonClicked;
			navItem.RightBarButtonItem.Clicked += cancelButtonClicked;
			searchBar.OnEditingStarted  += editStart;
			searchBar.OnEditingStopped += editEnd;
			SearchViewSource.RowTouched += searchRowTouched;
		}
		public override void ViewDidDisappear(bool animated)
		{
			//Remove our event delegates for memory management
			searchBar.SearchButtonClicked -= searchButtonClicked;
			navItem.RightBarButtonItem.Clicked -= cancelButtonClicked;
			SearchViewSource.RowTouched -= searchRowTouched;
			searchBar.OnEditingStarted -= editStart;
			searchBar.OnEditingStopped -= editEnd;
		}

		/// <summary>
		/// Search bar lost focus
		/// </summary>

		void editEnd (object sender, EventArgs e)
		{
			// if we are loosing focus due to the cancel button (not going to the next VC)
			if (!searchPressed && !toChild) {
				//setup main screen (same VC)
				advancedCancel.Title = "Advanced";
				NavigationController.SetNavigationBarHidden (false, false);
				navBar.Frame = new CGRect (0, 0, View.Frame.Width, navBarHeight);
				navBar.SetBackgroundImage (Resources.SearchBarBackgroundImage, UIBarMetrics.Default);
				mainViewSetup ();
			}
		}

		void editStart (object sender, EventArgs e)
		{
			//Hide the navigation bar, change button to Cancel
			if (navBar.Frame.Height != navBarHeight + UIApplication.SharedApplication.StatusBarFrame.Size.Height && !toChild) {
				advancedCancel.Title = "Cancel";
				searchBar.BecomeFirstResponder ();
				NavigationController.SetNavigationBarHidden (true, true);
				navBar.Frame = new CGRect (0, 0, View.Frame.Width, navBarHeight + 20);
				if (Convert.ToInt16(s.Split('.')[0]) > 6) 
					navBar.SetBackgroundImage (Resources.NavBarBackgroundImage, UIBarMetrics.Default);
				else 
					navBar.SetBackgroundImage (Resources.SearchBarBackgroundImage, UIBarMetrics.Default);
				// Adjust the frame to compensate for the lack of Navigation Bar
				mainView.Frame = new CGRect (0, searchHeight - navBarHeight, View.Frame.Width, View.Frame.Height - (searchHeight + navBarHeight));
				if (!tableShowing)
					clearMainView ();
			}

		}

		/// <summary>
		/// sets up the UI elements of main view as you enter the application 
		/// </summary>
		public void mainViewSetup()
		{
			// Clear any previous settings which may be coming in, ensure 'Default' state
			clearMainView ();
			mainView.Frame = new CGRect(0, mainViewHeight, this.View.Frame.Width, this.View.Frame.Width - mainViewHeight);
			searchBar.ResignFirstResponder ();
			searchBar.Text = "";
			advancedCancel.Title = "Advanced";
			tableShowing = false;

			//Figure out phone height, display appropriate image (tall for iPhone 4+ or regular)
			if (View.Frame.Height > 500f) {
				placeholder = new UIImageView (Resources.SearchBackgroundPlaceHolderTall);
				placeholder.Frame = new CGRect (0, 0, Resources.SearchBackgroundPlaceHolderTall.Size.Width, Resources.SearchBackgroundPlaceHolderTall.Size.Height);
			} else {
				placeholder = new UIImageView (Resources.SearchBackgroundPlaceHolder);
				placeholder.Frame = new CGRect (0, 0, Resources.SearchBackgroundPlaceHolder.Size.Width, Resources.SearchBackgroundPlaceHolder.Size.Height);
			}
			mainView.AddSubview (placeholder);

			//Description of advanced button
			searchHint = new UIImageView (Resources.SearchBarHint);
			searchHint.Frame = new CGRect (95, 20, Resources.SearchBarHint.Size.Width, Resources.SearchBarHint.Size.Height);
			mainView.AddSubview (searchHint);

			//UI Asthetic
			shadowView = new UIView ();
			shadowView.Frame = new CGRect (0, 0, View.Frame.Width, 1);
			shadowView.BackgroundColor = UIColor.FromRGBA (70, 70, 70, 26);
			mainView.AddSubview (shadowView);

		}

		private void searchRowTouched (object sender, TouchedEventArgs e)
		{
			// Deselect the row and fire the segue to the next VC, passing the person object
			personTouched = e.person;
			rowPressed = e.index;
			table.DeselectRow (e.index, false);
			this.PerformSegue("detailSegue", this);
		}


		public void searchButtonClicked (object sender, EventArgs e)
		{
			//Indicate search was pressed, setup to go to next VC
			searchPressed = true;
			searchText = searchBar.Text; 
			searchBar.ResignFirstResponder();

			// Get the formatted string for sharepoint
			
			GetSearchString gss = new GetSearchString();
			string s = gss.getSearchString("", "", "", "", "", "",  searchText.Trim(), LoadNicknames.allNicknames);

			//	Loading Dialog
			alert = new UIAlertView ();
			alert.Title = "Searching\nPlease Wait...";
			alert.AddButton ("Cancel");
			alert.Show ();

			//Search sharepoint, pass in searchText for exact matches
			new SearchSharePoint(s, searchText.Trim());
			searchPressed = false;

			alert.Dismissed += (object sende, UIButtonEventArgs even) => {
				try{
					searchBar.Text = "";
					if (results == null)
						searchBar.BecomeFirstResponder();
					SearchSharePoint.stop();
				}
				catch(Exception){}
			};
			//Search completed
			SearchSharePoint.searchComplete += (object send, SearchEventArgs ev) => {
				//Run on UI Thread
				InvokeOnMainThread (delegate { 
					//our search results as a List of Person! 
					results = ev.persons;
					// If no people were found
					if (ev.persons.Count == 0){
						// Error message
						clearMainView();
						UILabel label = new UILabel();
						label.Text = "No Results Found\n Please Try Again";
						label.Lines = 0;
						label.Font = UIFont.FromName("HelveticaNeue", 24f);
						label.TextColor = UIColor.FromRGB(70,70,70);
						label.Frame = new CGRect(View.Frame.Width/2 - 85, mainViewHeight + 18, mainView.Frame.Width,60);
						mainView.Add(label);
					}
					else{
						//Display the results in our table using SearchViewSource for setup.

						table = new UITableView(new CGRect(0, 18, mainView.Frame.Width, View.Frame.Height  - (searchHeight - navBarHeight + 18)), UITableViewStyle.Plain);
						mainView.Frame = new CGRect(0,mainView.Frame.Y,mainView.Frame.Width, mainView.Frame.Height + 35);
						clearMainView();
						// Add the table we created to our mainView.
						mainView.Add(table);
						advancedCancel.Title = "Cancel";
						// UI custom separator
						table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
						table.Source = new SearchViewSource (ev.persons);
						tableShowing = true;
					}
					// remove the searching popup
					alert.DismissWithClickedButtonIndex(0,true);
				});
			};


		}
		public void cancelButtonClicked (object sender, EventArgs e)
		{
			// if the button in its current state is Cancel
			if (advancedCancel.Title.Equals ("Cancel")) {
				// set up main screen to appear to 'Go Back'
				mainViewSetup ();

				//ensure the navigation bar is back, adjust the mainView and Nav Bar heights as needed
				NavigationController.SetNavigationBarHidden (false, false);
				navBar.Frame = new CGRect (0, mainViewHeight - navBarHeight, View.Frame.Width, navBarHeight);
				navBar.SetBackgroundImage (Resources.SearchBarBackgroundImage, UIBarMetrics.Default);
				mainView.Frame = new CGRect (0, mainViewHeight, View.Frame.Width, View.Frame.Height - (mainViewHeight));

			} else {
				// The buttons title is Advanced, go to the Advanced Controller
				advancedCancel.Title = "Advanced";
				tableShowing = false;
				if (advancedViewController == null) {
					this.advancedViewController = Storyboard.InstantiateViewController ("advancedController") as AdvancedViewController;
				}
				NavigationController.PushViewController (advancedViewController, false);
				searchBar.ResignFirstResponder ();
			}
		}
	
		public void clearMainView()
		{
			// Remove all the subViews from mainView.
			foreach (UIView view in mainView) {
				view.RemoveFromSuperview ();
			}
		}

		/// <summary>
		/// Expects the width of the label.
		/// </summary>
		/// <returns>The label width.</returns>
		/// <param name="text">Label text to be determined</param>
		/// <param name="fontSize">Font size.</param>
		private int expectedLabelWidth(NSString text, int fontSize)
		{
			UIFont font = UIFont.SystemFontOfSize(fontSize);
			var constraintSize = new CGSize(0, float.MaxValue);
			CGSize labelSize = text.StringSize(font, constraintSize, UILineBreakMode.WordWrap);
			return (int)labelSize.Width;
		}



		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			if (segue.Identifier.Equals("detailSegue")) {
				var vc = segue.DestinationViewController as ProfileViewController;

				if (vc != null) {
					vc.person = personTouched;
					vc.sendingController = this;
					toChild = true;
				}

			}
		}

	}
}
