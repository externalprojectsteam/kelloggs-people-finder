using PeopleFinder.Shared;
using UIKit;
using MessageUI;
using System;
using Foundation;
using System.Collections.Generic;
using CoreGraphics;
using MonoTouch.Dialog.Utilities;
using AddressBookUI;
using AddressBook;

namespace PeopleFinder.iOS
{
	public partial class ProfileViewController : UIViewController, IImageUpdated
	{

		public Person person;
		MFMailComposeViewController _mailController;
		nfloat mainHeight = 0;
		UIButton rightSegment, leftSegment, profileImage;
		UITableView moreInfoTable;
		UIScrollView moreInfoView;
		UIView shadowView2;
		UIBarButtonItem addToContacts;
		ABNewPersonViewController _newPersonController;
		string imageUrl;
		nfloat mainViewHeight;
		public UIViewController sendingController;

		public ProfileViewController (IntPtr handle) : base (handle)
		{
		}

		public void UpdatedImage (Uri uri)
		{
			var u = new Uri (imageUrl);
			//Because we are using a button, set the profile image for Normal and Selected states
			profileImage.SetImage(ImageLoader.DefaultRequestImage(u,this), UIControlState.Normal);
			profileImage.SetImage(ImageLoader.DefaultRequestImage(u,this), UIControlState.Selected);
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			_newPersonController = new ABNewPersonViewController ();
			NavigationController.SetNavigationBarHidden (false, true);

			//Find Device Version
			string s = (UIDevice.CurrentDevice.SystemVersion);


			// For iOS 7 
			if (Convert.ToInt16(s.Split('.')[0]) > 6) {
				mainViewHeight = UIApplication.SharedApplication.StatusBarFrame.Height + NavigationController.NavigationBar.Frame.Height;
				this.NavigationController.NavigationBar.TintColor = UIColor.White;

				nfloat backSize = Resources.BackButton.Size.Width + expectedLabelWidth (new NSString ("Back"), 17) + 15;
				UIView titleView = new UIView (new CGRect (0, 0, 300, 40));
				UILabel title;
				if (expectedLabelWidth (new NSString (person.fullName), 17) > View.Frame.Width - 150) {
					title = new UILabel (new CGRect ((View.Frame.Width - 150) / 2 - backSize, 7, View.Frame.Width - 150, Resources.NavBarLogo.Size.Height));
				} else
					title = new UILabel (new CGRect ((View.Frame.Width / 2 - (expectedLabelWidth (new NSString (person.fullName), 17) / 2) - backSize), 7, View.Frame.Width - 150, Resources.NavBarLogo.Size.Height));
				title.Text = person.fullName;
				title.Lines = 0;
				title.TextColor = UIColor.FromRGB (255, 255, 255);
				title.Font = UIFont.FromName ("HelveticaNeue", 17f);
				titleView.AddSubview (title);
				NavigationItem.TitleView = titleView;
		
			} else {
				mainViewHeight = 0;
				Title = person.fullName;
				//format back button
				UIView backButtonFrame = new UIView (new CGRect(0,0,100, Resources.BackButton.Size.Height));
				UIImageView backImage = new UIImageView (Resources.BackButton);
				backImage.Frame = new CGRect (0, 0, Resources.BackButton.Size.Width, Resources.BackButton.Size.Height);
				UILabel backText = new UILabel ();
				backText.Font = UIFont.FromName ("HelveticaNeue", 17f);
				backText.Text = "Back";
				backText.BackgroundColor = UIColor.Clear;
				backText.TextColor = UIColor.FromRGB (255, 255, 255);
				backText.Frame = new CGRect (backImage.Frame.Width + 5, 0, 50, Resources.BackButton.Size.Height);
				backButtonFrame.Add (backImage);
				backButtonFrame.Add (backText);

				NavigationItem.SetHidesBackButton (true, false);

				NavigationItem.LeftBarButtonItem = new UIBarButtonItem (backButtonFrame);

				UITapGestureRecognizer gesture;
				gesture = new UITapGestureRecognizer ();
				gesture.NumberOfTouchesRequired = 1;
				gesture.AddTarget(() => { NavigationController.PopToViewController(sendingController, true); });
				backButtonFrame.AddGestureRecognizer (gesture);
			}
			mainView.Frame = new CGRect(0, mainViewHeight, View.Frame.Width, View.Frame.Height - mainViewHeight);
			imageUrl = person.PictureURLWithAuth;

			profileImage = new UIButton ();
			profileImage.Frame = (new CGRect (12, 24, 72, 72));
			if (!imageUrl.Contains ("avatar")) {
				var u = new Uri (imageUrl);
				profileImage.SetImage (ImageLoader.DefaultRequestImage (u, this), UIControlState.Normal);
				profileImage.SetImage (ImageLoader.DefaultRequestImage (u, this), UIControlState.Selected);
			} else {
				profileImage.SetImage (UIImage.FromFile (imageUrl), UIControlState.Normal);
				profileImage.SetImage (UIImage.FromFile (imageUrl), UIControlState.Selected);
			}
			profileImage.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			profileImage.Layer.BorderColor = UIColor.FromRGB (200, 200, 200).CGColor;
			profileImage.Layer.BorderWidth = 1;
			mainView.AddSubview (profileImage);

			UILabel nameLabel = new UILabel ();
			nameLabel.Lines = 0;
			nameLabel.Text = person.fullName;
			nameLabel.TextColor = UIColor.FromRGB (226, 0, 52);
			nameLabel.Font = UIFont.FromName ("HelveticaNeue", 20);
			if (expectedLabelHeight (new NSString (nameLabel.Text), 20, new CGSize(View.Frame.Width - 108,0)) > 23) 
				nameLabel.Frame = new CGRect (96, 25, this.View.Frame.Width - 108, expectedLabelHeight (new NSString (nameLabel.Text), 20, new CGSize (View.Frame.Width - 108, 0))+1);
			else
				nameLabel.Frame = new CGRect (96, 40, this.View.Frame.Width - 108, expectedLabelHeight(new NSString(nameLabel.Text), 20, new CGSize(View.Frame.Width - 108,0))+1); 
			mainView.AddSubview (nameLabel);

			UILabel titleLabel = new UILabel ();
			titleLabel.Text = person.title;
			titleLabel.Lines = 0;
			titleLabel.TextColor = UIColor.FromRGB (150, 150, 150);
			titleLabel.Font = UIFont.FromName ("HelveticaNeue", 15);
			if (expectedLabelHeight (new NSString (nameLabel.Text), 20, new CGSize(View.Frame.Width - 108,0)) > 23) 
				titleLabel.Frame = new CGRect (96, 25 + expectedLabelHeight(new NSString(nameLabel.Text), 20, new CGSize(View.Frame.Width - 108,0)), this.View.Frame.Width - 108, expectedLabelHeight(new NSString(titleLabel.Text),15,new CGSize(View.Frame.Width - 108,0))+1);
			else
				titleLabel.Frame = new CGRect (96, 40 + expectedLabelHeight(new NSString(nameLabel.Text), 20, new CGSize(View.Frame.Width - 108,0)), this.View.Frame.Width - 108, expectedLabelHeight(new NSString(titleLabel.Text),15,new CGSize(View.Frame.Width - 108,0))+1);
			mainView.AddSubview (titleLabel);
			mainHeight = 120;

			UIView shadowView = new UIView ();
			shadowView.Frame = new CGRect (0, 0, View.Frame.Width, 1);
			shadowView.BackgroundColor = UIColor.FromRGBA (70, 70, 70, 26);
			mainView.AddSubview (shadowView);

			addToContacts = new UIBarButtonItem ();
			addToContacts.SetBackgroundImage (new UIImage (), UIControlState.Normal, UIBarMetrics.Default);
			addToContacts.Image = Resources.AddIcon;
			NavigationItem.RightBarButtonItem = addToContacts;
			addToContacts.Clicked += (object sender, EventArgs e) => {

				var abPerson = new ABPerson();
				abPerson.FirstName = person.firstName;
				abPerson.LastName = person.lastName;
				ABMutableMultiValue<String> phones = new ABMutableStringMultiValue();
				ABMutableMultiValue<String> emails = new ABMutableStringMultiValue();

				if (person.isWorkPhoneVisible)
					phones.Add(new NSString(person.workPhone), new NSString ("Work"));
				if (person.isMobilePhoneVisible)
					phones.Add(new NSString(person.mobilePhone), new NSString("Mobile"));
				abPerson.SetPhones(phones);
				if (!person.PictureURLWithAuth.Contains("avatar"))
				{
					var u = new Uri (imageUrl);
					abPerson.Image = ImageLoader.DefaultRequestImage(u,this).AsPNG();
				}
				if (person.isEmailVisible)
				{
					emails.Add(new NSString(person.email), new NSString("Work"));
				}
				abPerson.SetEmails(emails);
				abPerson.JobTitle = person.title;
				abPerson.Organization = "Kellogg Company";
				_newPersonController.DisplayedPerson = abPerson;
				NavigationController.PushViewController(_newPersonController, true);

			};
			_newPersonController.NewPersonComplete += (object sender,
			            ABNewPersonCompleteEventArgs e) => {

				if (e.Completed) {
					UIAlertView alert = new UIAlertView();
					alert.Title = "Contact Added";
					alert.AddButton("OK");
					alert.Show();
				}

				NavigationController.PopViewController (true);
			};

			if (person.isMoreInfoVisible) {
				showSegmented ();
			}
			showTable ();

		}


		public override void ViewDidAppear(bool animated)
		{
			ProfileCell.SMSTouched += textButton;
			ProfileCell.MailTouched += mailButton;
			ProfileTableSource.RowTouched += rowTouched;
			try{
				rightSegment.TouchUpInside += rightSegmentTouch;
				leftSegment.TouchUpInside += leftSegmentTouch;
			}
			catch(Exception){
			}
			profileImage.TouchUpInside += profileImageTouch;

		}
		public override void ViewDidDisappear(bool animated)
		{
			ProfileCell.SMSTouched -= textButton;
			ProfileCell.MailTouched -= mailButton;
			ProfileTableSource.RowTouched -= rowTouched;
			try{
				rightSegment.TouchUpInside -= rightSegmentTouch;
				leftSegment.TouchUpInside -= leftSegmentTouch;
			}catch (Exception){
			}
			profileImage.TouchUpInside -= profileImageTouch;


		}
		private void profileImageTouch (object sender, EventArgs e)
		{
			this.PerformSegue ("pictureLarge", this);
		}
		private void rightSegmentTouch (object sender, EventArgs e)
		{
			if (!rightSegment.Selected) {
				rightSegment.Selected = true;
				leftSegment.Selected = false;
				moreInfoTable.RemoveFromSuperview ();
				showMoreInfo ();

			}
		}
		private void leftSegmentTouch (object sender, EventArgs e)
		{
			if (!leftSegment.Selected) {
				leftSegment.Selected = true;
				rightSegment.Selected = false;
				moreInfoView.RemoveFromSuperview ();
				shadowView2.RemoveFromSuperview ();
				showTable ();

			}

		}
		private void showTable(){
			
			moreInfoTable = new UITableView(new CGRect (0, mainHeight, View.Frame.Width, mainView.Frame.Height));
			moreInfoTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			List<CellInfo> cells = new List<CellInfo> ();
			if (person.isMobilePhoneVisible) {
				cells.Add (new CellInfo ("Mobile", person.mobilePhone));
			}
			if (person.isWorkPhoneVisible) {
				cells.Add (new CellInfo ("Work", person.workPhone));
			}
			if (person.isEmailVisible) {
				cells.Add (new CellInfo ("Email", person.email));
			}
			if (person.isLocationVisible) {
				cells.Add (new CellInfo ("Location", person.location));
			}
			moreInfoTable.Source = new ProfileTableSource (cells);
			mainView.AddSubview(moreInfoTable);
		}

		private void showMoreInfo(){
			int lineHeightSpacing = 30;
			moreInfoView = new UIScrollView (new CGRect (0, mainHeight, View.Frame.Width, mainView.Frame.Height - mainHeight));
			moreInfoView.AlwaysBounceHorizontal = false;
			mainView.Add (moreInfoView);
			shadowView2 = new UIView ();
			shadowView2.Frame = new CGRect (0, mainHeight-1, View.Frame.Width, 1);
			shadowView2.BackgroundColor = UIColor.FromRGBA (70, 70, 70, 26);
			mainView.AddSubview (shadowView2);
			float y = 10;
			if (person.isAboutMeVisibile) {
				var aboutmeLabel = new UILabel (new CGRect (12, y, View.Frame.Width - 24, 30));
				moreInfoView.AddSubview (aboutmeLabel);
				aboutmeLabel.Font = UIFont.FromName ("HelveticaNeue", 14);
				aboutmeLabel.TextColor = UIColor.FromRGB (226, 0, 52);
				aboutmeLabel.Text = "About";


				y += lineHeightSpacing;
				var aboutMe = new UILabel (new CGRect (12, y, View.Frame.Width - 24, 30));
				moreInfoView.AddSubview (aboutMe);
				aboutMe.Text = person.aboutme;
				aboutMe.Font = UIFont.FromName ("HelveticaNeue", 15);
				aboutMe.LineBreakMode = UILineBreakMode.WordWrap;
				aboutMe.TextColor = UIColor.FromRGB (70, 70, 70);
				aboutMe.Lines = 0;
				aboutMe.SizeToFit ();

				y += expectedLabelHeight (new NSString (aboutMe.Text), 15, aboutMe.Frame.Size) + 13;
			}

			if (person.isSkillsVisible) {
				UIImageView separator1 = new UIImageView (new CGRect(12, y, View.Frame.Width-24, 1));
				separator1.Image = Resources.Separator;
				moreInfoView.AddSubview (separator1);
				y += 15;

				var skillsLabel = new UILabel (new CGRect (12, y, View.Frame.Width - 24, 30));
				moreInfoView.AddSubview (skillsLabel);
				skillsLabel.Font = UIFont.FromName ("HelveticaNeue", 14);
				skillsLabel.TextColor = UIColor.FromRGB (226, 0, 52);
				skillsLabel.Text = "Skills";


				y += lineHeightSpacing;
				var skills = new UILabel (new CGRect (12, y, View.Frame.Width - 24, 30));
				moreInfoView.AddSubview (skills);
				skills.Text = person.skills;
				skills.Font = UIFont.FromName ("HelveticaNeue", 15);
				skills.LineBreakMode = UILineBreakMode.WordWrap;
				skills.TextColor = UIColor.FromRGB (70, 70, 70);
				skills.Lines = 0;
				skills.SizeToFit ();

				y += expectedLabelHeight (new NSString (skills.Text), 15, skills.Frame.Size) + 13;
			}
			if (person.isAskMeAboutVisible) {
				UIImageView separator2 = new UIImageView (new CGRect(12, y, View.Frame.Width-24, 1));
				separator2.Image = Resources.Separator;
				moreInfoView.AddSubview (separator2);
				y += 15;

				var askMeAboutLabel = new UILabel (new CGRect (12, y, View.Frame.Width - 24, 30));
				moreInfoView.AddSubview (askMeAboutLabel);
				askMeAboutLabel.Font = UIFont.FromName ("HelveticaNeue", 14);
				askMeAboutLabel.TextColor = UIColor.FromRGB (226, 0, 52);
				askMeAboutLabel.Text = "Ask Me About";


				y += lineHeightSpacing;
				var askMeAbout = new UILabel (new CGRect (12, y, View.Frame.Width - 24, 30));
				moreInfoView.AddSubview (askMeAbout);
				askMeAbout.Text = person.askmeabout;
				askMeAbout.Font = UIFont.FromName ("HelveticaNeue", 15);
				askMeAbout.LineBreakMode = UILineBreakMode.WordWrap;
				askMeAbout.TextColor = UIColor.FromRGB (70, 70, 70);
				askMeAbout.Lines = 0;
				askMeAbout.SizeToFit ();
				y += expectedLabelHeight (new NSString (askMeAbout.Text), 15, askMeAbout.Frame.Size) + 13;

			}
			y += lineHeightSpacing;
			moreInfoView.ContentSize = new CGSize (View.Frame.Width-24, y);
		}
		private void showSegmented()
		{
			leftSegment = new UIButton (new CGRect (60, 120, Resources.LeftSegmented.Size.Width, Resources.LeftSegmented.Size.Height));
			leftSegment.SetBackgroundImage (Resources.LeftSegmented, UIControlState.Normal);
			leftSegment.SetBackgroundImage (Resources.LeftSegmentedSelected, UIControlState.Highlighted);
			leftSegment.SetBackgroundImage (Resources.LeftSegmentedSelected, UIControlState.Selected);
			leftSegment.SetBackgroundImage (Resources.LeftSegmentedSelected, UIControlState.Reserved);
			leftSegment.Selected = true;
			leftSegment.SetTitle ("Profile", UIControlState.Normal);
			leftSegment.SetTitle ("Profile", UIControlState.Selected);
			leftSegment.AdjustsImageWhenHighlighted = false;
			leftSegment.SetTitleColor(UIColor.FromRGB (255, 255, 255), UIControlState.Selected);
			leftSegment.SetTitleColor(UIColor.FromRGB (226, 0, 52), UIControlState.Normal);
			leftSegment.SetTitleColor (UIColor.FromRGB (255, 255, 255), UIControlState.Highlighted);
			leftSegment.Font = UIFont.FromName ("HelveticaNeue", 13);

			rightSegment = new UIButton (new CGRect (60 + Resources.LeftSegmented.Size.Width, 120, Resources.RightSegmented.Size.Width, Resources.RightSegmented.Size.Height));
			rightSegment.SetBackgroundImage (Resources.RightSegmented, UIControlState.Normal);
			rightSegment.SetBackgroundImage (Resources.RightSegmentedSelected, UIControlState.Highlighted);
			rightSegment.SetBackgroundImage (Resources.RightSegmentedSelected, UIControlState.Selected);
			rightSegment.SetTitle ("About", UIControlState.Normal);
			rightSegment.SetTitle ("About", UIControlState.Selected);
			rightSegment.SetTitleColor(UIColor.FromRGB (255, 255, 255), UIControlState.Selected);
			rightSegment.SetTitleColor(UIColor.FromRGB (226, 0, 52), UIControlState.Normal);
			rightSegment.SetTitleColor (UIColor.FromRGB (255, 255, 255), UIControlState.Highlighted);
			rightSegment.Font = UIFont.FromName ("HelveticaNeue", 13);
			rightSegment.AdjustsImageWhenHighlighted = false;

			mainView.AddSubview (leftSegment);
			mainView.AddSubview (rightSegment);
			mainHeight += Resources.LeftSegmented.Size.Height + 12;
		}


		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			if (segue.Identifier.Equals("pictureLarge")) {
				var vc = segue.DestinationViewController as ImageViewController;

				if (vc != null) {
					vc.imageSource = person.PictureURLWithAuth;
					vc.sendingViewController = this;
				}
			}
		}
		private int expectedLabelWidth(NSString text, int fontSize)
		{
			UIFont font = UIFont.SystemFontOfSize(fontSize);
			var constraintSize = new CGSize(0, float.MaxValue);
			CGSize labelSize = text.StringSize(font, constraintSize, UILineBreakMode.WordWrap);
			return (int)labelSize.Width;
		}
		private int expectedLabelHeight(NSString text, int fontSize, CGSize size)
		{
			UIFont font = UIFont.SystemFontOfSize(fontSize);
			var constraintSize = new CGSize(size.Width, float.MaxValue);
			CGSize labelSize = text.StringSize(font, constraintSize, UILineBreakMode.WordWrap);
			return (int)labelSize.Height;
		}
		private void textButton(object sender, MessageEventArgs e){
			PresentViewController (e.controller, true, null);
		}
		private void mailButton(object sender, MessageEventArgs e){
			PresentViewController (e.cont, true, null);
		}
		private void rowTouched (object sender, ProfileEventArgs ev){
			CellInfo cellProperties = ev.cellInfo;
			if (cellProperties.cellTitle.Equals("Mobile") || cellProperties.cellTitle.Equals("Work"))
			{
				string phonenum = cellProperties.cellInfo;
				string newPhone = "";
				for (int i = 0; i < phonenum.Length; i++) {
					if (char.IsDigit (phonenum [i])) {
						newPhone += phonenum [i];
					}
				}
				var phoneTo = NSUrl.FromString("telprompt://" + newPhone);
				if (UIApplication.SharedApplication.CanOpenUrl(phoneTo)) {
					UIApplication.SharedApplication.OpenUrl(phoneTo);
				} else {
					Console.WriteLine("Cannot open Phone");
				}
				moreInfoTable.DeselectRow(ev.index, false);
			}
			else if (cellProperties.cellTitle.Equals("Email") && MFMailComposeViewController.CanSendMail)
			{
				_mailController = new MFMailComposeViewController ();
				_mailController.SetToRecipients (new string[]{cellProperties.cellInfo});
				_mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
					args.Controller.DismissViewController (true, null);
					moreInfoTable.DeselectRow(ev.index, false);

				};
				this.PresentViewController (_mailController, true, null);
			}
			else
				moreInfoTable.DeselectRow(ev.index, false);

		}
	}
}
