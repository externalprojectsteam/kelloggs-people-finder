// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using UIKit;
using System;
using System.CodeDom.Compiler;

namespace PeopleFinder.iOS
{
	[Register ("ImageViewController")]
	partial class ImageViewController
	{
		[Outlet]
		UIKit.UIImageView profileImageLarge { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
