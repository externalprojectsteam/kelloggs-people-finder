using UIKit;
using System;
using Foundation;
using CoreGraphics;
using MonoTouch.Dialog.Utilities;


namespace PeopleFinder.iOS
{
	public partial class ImageViewController : UIViewController, IImageUpdated
	{
		public string imageSource = "";
		public UIViewController sendingViewController;
		public ImageViewController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();
			if (imageSource.Contains ("avatar")) {
				profileImageLarge.Image = UIImage.FromFile (imageSource);
			} else {
				var u = new Uri (imageSource);
				profileImageLarge.Image = ImageLoader.DefaultRequestImage (u, this);
			}

			//Find Device Version
			string s = (UIDevice.CurrentDevice.SystemVersion);


			// For iOS 7
			if (Convert.ToInt16(s.Split('.')[0]) > 6) {
				nfloat backSize = Resources.BackButton.Size.Width + expectedLabelWidth (new NSString ("Back"), 17) + 15;
				UIView titleView = new UIView (new CGRect (0, 0, 300, 40));
				UILabel title = new UILabel (new CGRect ((View.Frame.Width / 2 - (expectedLabelWidth (new NSString ("Profile Image"), 17) / 2) - backSize), 7, expectedLabelWidth (new NSString ("Profile Image"), 17), Resources.NavBarLogo.Size.Height));
				title.Text = "Profile Image";
				title.TextColor = UIColor.FromRGB (255, 255, 255);
				title.Font = UIFont.FromName ("HelveticaNeue", 17f);
				titleView.AddSubview (title);
				NavigationItem.TitleView = titleView;
			} else {
				profileImageLarge.Frame = new CGRect(25, 50, View.Frame.Width - 25, View.Frame.Height - 50);
				Title = "Profile Image";
				UIView backButtonFrame = new UIView (new CGRect(0,0,100, Resources.BackButton.Size.Height));
				UIImageView backImage = new UIImageView (Resources.BackButton);
				backImage.Frame = new CGRect (0, 0, Resources.BackButton.Size.Width, Resources.BackButton.Size.Height);
				UILabel backText = new UILabel ();
				backText.Font = UIFont.FromName ("HelveticaNeue", 17f);
				backText.Text = "Back";
				backText.BackgroundColor = UIColor.Clear;
				backText.TextColor = UIColor.FromRGB (255, 255, 255);
				backText.Frame = new CGRect (backImage.Frame.Width + 5, 0, 50, Resources.BackButton.Size.Height);
				backButtonFrame.Add (backImage);
				backButtonFrame.Add (backText);

				NavigationItem.SetHidesBackButton (true, false);

				NavigationItem.LeftBarButtonItem = new UIBarButtonItem (backButtonFrame);

				UITapGestureRecognizer gesture;
				gesture = new UITapGestureRecognizer ();
				gesture.NumberOfTouchesRequired = 1;
				gesture.AddTarget(() => { NavigationController.PopToViewController(sendingViewController, true); });
				backButtonFrame.AddGestureRecognizer (gesture);
			}

		}
		public void UpdatedImage (Uri uri)
		{
			var u = new Uri (imageSource);
			profileImageLarge.Image = ImageLoader.DefaultRequestImage(u,this);
		}
		private int expectedLabelWidth(NSString text, int fontSize)
		{
			UIFont font = UIFont.SystemFontOfSize(fontSize);
			var constraintSize = new CGSize(0, float.MaxValue);
			CGSize labelSize = text.StringSize(font, constraintSize, UILineBreakMode.WordWrap);
			return (int)labelSize.Width;
		}
	}
}
