// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace PeopleFinder.iOS
{
    [Register ("RegularSearchViewController")]
    partial class RegularSearchViewController
    {
        [Outlet]
        UIKit.UIView mainView { get; set; }


        [Outlet]
        UIKit.UINavigationBar navBar { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (navBar != null) {
                navBar.Dispose ();
                navBar = null;
            }
        }
    }
}