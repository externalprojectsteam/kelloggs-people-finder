using System;
using UIKit;

namespace PeopleFinder.iOS
{
	public class Resources
	{
		public static UIImage NavBarBackgroundImage = UIImage.FromFile("Images/bg_navbar.png");
		public static UIImage NavBarLogo = UIImage.FromFile("Images/logo_k.png");
		public static UIImage NavSearchBar = UIImage.FromFile ("Images/bg_navsearchbar.png");
		public static string NavBarText = "People Finder";
		public static UIImage SearchBackgroundPlaceHolder = UIImage.FromFile ("Images/logo_placeholder.png");
		public static UIImage SearchBackgroundPlaceHolderTall = UIImage.FromFile ("Images/logo_placeholder-568h@2x.png");
		public static UIImage SearchBarBackgroundImage = UIImage.FromFile("Images/bg_searchbar.png");
		public static UIImage SearchBarHint = UIImage.FromFile("Images/hint.png");
		public static UIImage BackButton = UIImage.FromFile("Images/ico_back.png");
		public static UIImage HandleStraight = UIImage.FromFile("Images/handle_straight.png");
		public static UIImage Separator = UIImage.FromFile("Images/separator.png");
		public static UIImage SearchLoginButton = UIImage.FromFile("Images/btn_std.png");
		public static UIImage SearchLoginButtonPressed = UIImage.FromFile("Images/btn_prd.png");
		public static UIImage HandleUpwards = UIImage.FromFile("Images/handle_upward.png");
		public static UIImage RightSegmented = UIImage.FromFile("Images/segmented_r_std.png");
		public static UIImage RightSegmentedSelected = UIImage.FromFile("Images/segmented_r_sel.png");
		public static UIImage LeftSegmented = UIImage.FromFile("Images/segmented_l_std.png");
		public static UIImage LeftSegmentedSelected = UIImage.FromFile("Images/segmented_l_sel.png");
		public static UIImage CallIcon = UIImage.FromFile("Images/ico_call.png");
		public static UIImage EmailIcon = UIImage.FromFile("Images/ico_email.png");
		public static UIImage TextIcon = UIImage.FromFile("Images/ico_text.png");
		public static UIImage AddIcon = UIImage.FromFile("Images/ico_add.png");
		public Resources ()
		{
		}
	}
}

