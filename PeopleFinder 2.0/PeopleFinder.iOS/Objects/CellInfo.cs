
namespace PeopleFinder.iOS
{
	public class CellInfo
	{
		public string cellTitle { get; set; }
		public string cellInfo { get; set; }
		public CellInfo (string cellTitle, string cellInfo)
		{
			this.cellInfo = cellInfo;
			this.cellTitle = cellTitle;
		}
	}
}

