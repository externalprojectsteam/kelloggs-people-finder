using System;
using PeopleFinder.Shared;
using Foundation;


namespace PeopleFinder.iOS
{
	public class TouchedEventArgs : EventArgs
	{
		public TouchedEventArgs (Person person, NSIndexPath index)
		{
			this.person = person;
			this.index = index;
		}
		public Person person{get; set;}
		public NSIndexPath index { get; set; }
	}
}

