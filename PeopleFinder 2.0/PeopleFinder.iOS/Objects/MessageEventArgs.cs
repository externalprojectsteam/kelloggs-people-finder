using System;
using MessageUI;


namespace PeopleFinder.iOS
{
	public class MessageEventArgs:EventArgs
	{
		public MFMessageComposeViewController controller {get; set;}
		public MFMailComposeViewController cont {get; set;}
		public MessageEventArgs (MFMessageComposeViewController controller, MFMailComposeViewController cont)
		{
			this.controller = controller;
			this.cont = cont;
		}
	}
}

