using System;
using Foundation;


namespace PeopleFinder.iOS
{
	public class ProfileEventArgs:EventArgs
	{
		public CellInfo cellInfo { get; set;}
		public NSIndexPath index { get; set; }
		public ProfileEventArgs (CellInfo cellInfo, NSIndexPath index)
		{
			this.cellInfo = cellInfo;
			this.index = index;

		}
	}
}

