// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PeopleFinder.iOS
{
	[Register ("ProfileCell")]
	partial class ProfileCell
	{
		[Outlet]
		UIKit.UILabel cellInfo { get; set; }

		[Outlet]
		UIKit.UILabel cellTitle { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (cellTitle != null) {
				cellTitle.Dispose ();
				cellTitle = null;
			}

			if (cellInfo != null) {
				cellInfo.Dispose ();
				cellInfo = null;
			}
		}
	}
}
