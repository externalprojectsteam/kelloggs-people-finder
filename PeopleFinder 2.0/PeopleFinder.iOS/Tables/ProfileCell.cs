using UIKit;
using Foundation;
using MessageUI;
using System;
using CoreGraphics;

namespace PeopleFinder.iOS
{
	public partial class ProfileCell:UITableViewCell
	{
		public static readonly NSString Key = new NSString ("ProfileCell");
		UIButton textButton = new UIButton();
		UIButton mobilephoneButton = new UIButton ();
		UIButton callphoneButton = new UIButton();
		MFMailComposeViewController _mailController;
		UIButton emailButton = new UIButton();
		MFMessageComposeViewController controller;

		public static event EventHandler<ProfileEventArgs> RowTouched;
		public static event EventHandler<MessageEventArgs> SMSTouched;
		public static event EventHandler<MessageEventArgs> MailTouched;


		public ProfileCell () : base (UITableViewCellStyle.Value1, Key)
		{
			// TODO: add subviews to the ContentView, set various colors, etc.
			//TextLabel.Text = "TextLabel";

			SelectionStyle = UITableViewCellSelectionStyle.Gray;
			UIImageView separator = new UIImageView (new CGRect(12, 63, this.Frame.Width-24, 1));
			separator.Image = Resources.Separator;
			ContentView.AddSubview (separator);

			cellTitle = new UILabel () {
				Font = UIFont.FromName("HelveticaNeue", 14f),
				TextColor = UIColor.FromRGB (226, 0, 52),
				BackgroundColor = UIColor.Clear
			};
			cellInfo = new UILabel () {
				Font = UIFont.FromName("HelveticaNeue", 18f),
				TextColor = UIColor.FromRGB (0, 0, 0),
				BackgroundColor = UIColor.Clear
			};

			ContentView.Add (cellTitle);
			ContentView.Add (cellInfo);
			ContentView.Add (textButton);
			ContentView.Add (mobilephoneButton);
			ContentView.Add (callphoneButton);
			ContentView.Add (emailButton);
		}

		public void setCellText(string title, string info)
		{
			cellTitle.Text = title;
			cellInfo.Text = info;
		
			if (title.Equals("Email")){
				emailButton.SetImage (Resources.EmailIcon, UIControlState.Normal);
				emailButton.TouchUpInside += (object sender, EventArgs e) => {
					_mailController = new MFMailComposeViewController ();
					_mailController.SetToRecipients (new string[]{cellInfo.Text});
					_mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
						args.Controller.DismissViewController (true, null);

					};
					MailTouched(this, new MessageEventArgs(null, _mailController));
				};
			}
		
			if (title.Equals ("Mobile")) {
				textButton.SetImage (Resources.TextIcon, UIControlState.Normal);
				textButton.TouchUpInside += (object sender, EventArgs e) => {
					controller = new MFMessageComposeViewController ();
					if(MFMessageComposeViewController.CanSendText)
					{
						controller.Recipients = new string[]{info};
						controller.Finished += ( object s, MFMessageComposeResultEventArgs args) => {
							args.Controller.DismissViewController (true, null);
						};
						SMSTouched(this, new MessageEventArgs(controller, null));

					}
				};
				mobilephoneButton.SetImage (Resources.CallIcon, UIControlState.Normal);
				mobilephoneButton.TouchUpInside += (object send, EventArgs ev) => {
					RowTouched(this, new ProfileEventArgs(new CellInfo(cellTitle.Text, cellInfo.Text), NSIndexPath.FromIndex(0)));
				};
			}
			if (title.Equals ("Work")) {
				callphoneButton.SetImage (Resources.CallIcon, UIControlState.Normal);
				callphoneButton.TouchUpInside += (object sender, EventArgs e) => {
					RowTouched(this, new ProfileEventArgs(new CellInfo(cellTitle.Text, cellInfo.Text), NSIndexPath.FromIndex(0)));
				};
			}

		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			cellTitle.Frame = new CGRect(12, 12, ContentView.Bounds.Width - 63, 17);
			cellInfo.Frame = new CGRect(12, 30, ContentView.Frame.Width - 24, 25);
			textButton.Frame = new CGRect (ContentView.Bounds.Width - 100, 15, 25, 25);
			mobilephoneButton.Frame = new CGRect (ContentView.Bounds.Width - 50, 15, 25, 25);
			callphoneButton.Frame = new CGRect (ContentView.Bounds.Width - 50, 15, 25, 25);
			emailButton.Frame = new CGRect (ContentView.Bounds.Width - 50, 15, 25, 25);
		}
	}
}

