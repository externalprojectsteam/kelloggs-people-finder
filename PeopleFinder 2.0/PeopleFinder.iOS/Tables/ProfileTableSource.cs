using UIKit;
using System.Collections.Generic;
using System;
using Foundation;

namespace PeopleFinder.iOS
{
	public class ProfileTableSource: UITableViewSource
	{
		List<CellInfo> cellsInfo;
		public static event EventHandler<ProfileEventArgs> RowTouched;
		public ProfileTableSource (List<CellInfo> cellsInfo)
		{
			this.cellsInfo = cellsInfo;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return cellsInfo.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			ProfileCell cell = tableView.DequeueReusableCell (ProfileCell.Key) as ProfileCell;
			// if there are no cells to reuse, create a new one
			if (cell == null)
				cell = new ProfileCell();
			cell.setCellText( cellsInfo [indexPath.Row].cellTitle, cellsInfo [indexPath.Row].cellInfo);
			return cell;
		}
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			RowTouched(this, new ProfileEventArgs(cellsInfo[indexPath.Row], indexPath));
		}
		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 64f;
		}
	}
}

