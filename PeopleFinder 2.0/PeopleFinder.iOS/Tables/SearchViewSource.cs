using System.Collections.Generic;
using UIKit;
using System;
using PeopleFinder.Shared;
using Foundation;


namespace PeopleFinder.iOS
{
	public class SearchViewSource : UITableViewSource
	{
		List<Person> persons;
		public static event EventHandler<TouchedEventArgs> RowTouched;

		public SearchViewSource (List<Person> persons)
		{
			this.persons = persons;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			// TODO: return the actual number of items in the section
			return persons.Count;
		}
	
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			ResultsCell cell = tableView.DequeueReusableCell (ResultsCell.Key) as ResultsCell;
			// if there are no cells to reuse, create a new one
			if (cell == null)
				cell = new ResultsCell ();
			if (persons [indexPath.Row].fullName.Contains ("\\")) {
				persons [indexPath.Row].fullName = persons [indexPath.Row].firstName + " " + persons [indexPath.Row].lastName;
			}

			cell.setCellText(persons[indexPath.Row].fullName, persons[indexPath.Row].title, persons [indexPath.Row].PictureURLWithAuth);
			return cell;
		}
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			//if(RowTouched != null)
			RowTouched(this, new TouchedEventArgs(persons[indexPath.Row], indexPath));
		}
		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 84f;
		}


	}
}

